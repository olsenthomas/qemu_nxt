/*
 * Ultra Chip uc1601 LCD Controller
 *
 * Written by Thomas Olsen <olsen.thomas@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 */
 
 /* According to the Nxt board specifications */

 // Display 128 x 64
// 1/65 duty, 1/9 bias
// VLCD 12.0V

// SPI interface
//
// PCB        LCD       ARM       PIO
// ------     -----     ----      -----
// CS_DIS     -CS1      PA10      NPCS2 (PB)
// DIS_A0     A0        PA12      PA12
// DIS_SCL    SCL       PA14      SPCK  (PA)
// DIS_SDA    SI        PA13      MOSI  (PA)

/* All pins are write only in serial s8 mode */
#define PIN_CS0             1	 	
#define PIN_CD              3
#define PIN_SCK             4
#define PIN_SDA             5
 
