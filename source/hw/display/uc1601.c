/*
 * Ultra Chip uc1601 LCD Controller
 *
 * Written by Thomas Olsen <olsen.thomas@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "ui/console.h"
#include "ui/pixel_ops.h"
#include "uc1601.h"
#include "../hw/arm/debug.h"

/* Only the 4-WIRE SERIAL INTERFACE (S8) is implemented  */
// Only MX = 0, MY = 0, SL = 0 is implemented!!!!!!

#define D(x) x
#define WIDTH 100
#define HEIGHT 64
#define CZOOM 8

#define uc1600_SetPageAddress 				0xB0
#define uc1600_SetColumnAddressMSB			0x10
#define uc1600_SetColumnAddressLSB			0x00
#define uc1600_SetSetVBIASPotentiometer 	0x81
#define uc1600_SetFrameRate					0xA0
#define uc1600_SetDisplayON					0xAE

#ifndef UC1601_ERR_DEBUG
#define UC1601_ERR_DEBUG 0
#endif

const char* pinName(int pin);

typedef struct UC1601State {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    QemuConsole *con;
    uint16_t byteCount;
    bool isDataCommand; // otherwise display command
    bool isChipSelected; // Is this chip selected as slave?
    uint8_t data[(HEIGHT/8)*CZOOM][WIDTH*CZOOM];
    uint8_t temp;
    uint16_t x;
    uint16_t y;
    uint8_t bitPosition;
    bool isNextCommandSetVBIASPotentiometer;
    uint8_t vBIASPotentiometer;
    uint8_t fps;
    bool isDisplayOn;
} UC1601State;

#define TYPE_UC1601 "uc1601"
#define UC1601(obj) \
    OBJECT_CHECK(UC1601State, (obj), TYPE_UC1601)

const char* pinName(int pin)
{
	switch (pin)
	{
		case PIN_CS0:
			return "uc1601_CSO";
		case PIN_CD:
			return "uc1601_CD";
		case PIN_SCK:
			return "uc1601_SCK";
		case PIN_SDA:
			return "uc1601_SDA";					
	}
	
	hw_error("uc1600: %s: pin = '%d' is not supported", __FUNCTION__, pin);
	return "";
}

static int uc1601_enabled(UC1601State *s)
{
        return s->isDisplayOn;
}

static void handleCommand(UC1601State *s)
{
	if (s->isNextCommandSetVBIASPotentiometer)
	{
		s->vBIASPotentiometer = s->temp;
		s->isNextCommandSetVBIASPotentiometer = false;
	}
	else if ((s->temp & 0xF0) == uc1600_SetPageAddress)
		s->y = (s->temp & 0xF)*CZOOM;
	else if ((s->temp & 0xF0) == uc1600_SetColumnAddressMSB)
		s->x = s->temp & 0xF;
	else if ((s->temp & 0xF0) == uc1600_SetColumnAddressLSB)
		;
	else if ((s->temp & 0xFE) == uc1600_SetDisplayON)
	{
			if (s->temp & 0x1)
				s->isDisplayOn = true;
			else
				s->isDisplayOn = false;
	}
	else if (s->temp == uc1600_SetSetVBIASPotentiometer)
		s->isNextCommandSetVBIASPotentiometer = true;
	else if ((s->temp & 0xFE) == uc1600_SetFrameRate)
	{
			if (s->temp & 0x01)
				s->fps = 95;
			else
				s->fps = 76;
	}
	else
    {
      debug_log_command_not_implemented(UC1601_ERR_DEBUG, __func__, s->temp);
	  //printf("%s: command ='0x%x' not implemented!!!q\n", __FUNCTION__, s->temp);				
    }	
}

static void uc1601_set_pin(void *opaque, int pin, int level)
{
   	UC1601State *s = opaque;	
    //printf("%s: %s level='%d'\n", __FUNCTION__, pinName(pin), level);
	
	switch (pin)
	{
		case PIN_CD:
			s->isDataCommand = level? true : false;
			//printf("%s: %s level='%d'\n", __FUNCTION__, pinName(pin), level);
			break;
		case PIN_CS0:
		    //printf("%s: %s level='%d'\n", __FUNCTION__, pinName(pin), level);
			s->isChipSelected = level? false:true;	
						
			if (!s->isChipSelected)
			{	
				//printf("%s: temp='0x%x' \n", __FUNCTION__, s->temp);	
				
				if (s->isDataCommand)
				{
					int i = 0, j = 0, z = 0;
					uint8_t expandedData[CZOOM];
					for (z = 0; z < CZOOM; z++)
					{
						if (s->temp & (1 << z))
							expandedData[z] = 0xFF;
						else
							expandedData[z] = 0x0;
					}

					for (j = s->y; j < (s->y+CZOOM); j++)
						for (i = s->x ; i < (s->x + CZOOM); i++)
							s->data[j][i] = expandedData[j-s->y];

					s->x += CZOOM;

					//printf("%s: x='%d' y='%d'\n", __FUNCTION__, s->x, s->y);
				}
				else
				{
					handleCommand(s);
				}
				
				s->bitPosition = 7;				
				s->temp = 0;
				s->byteCount++;
			}
							
			break;
		case PIN_SDA:
		    //printf("%s: %s level='%d'\n", __FUNCTION__, pinName(pin), level);
			s->temp |= (level << s->bitPosition);
			s->bitPosition -= 1;		
			break;						
	}

	if (s->byteCount == 300)
	{		
		//printf("%s: reset x and y\n", __FUNCTION__);
		s->byteCount = 0;
		graphic_hw_update(s->con);    
	}
}

static void uc1601_update_display(void *opaque)
{
        UC1601State *s = opaque;
        if (!uc1601_enabled(s)) {
		  return;
		}

        DisplaySurface *surface = qemu_console_surface(s->con);

        //printf("%s: called: height: %d width:%d stride: %d bits per pixel: %d\n", __FUNCTION__, surface_height(surface), surface_width(surface), surface_stride(surface), surface_bits_per_pixel(surface));

            		 		
      	uint16_t y,x;
		uint8_t* d;                


   		for (y = 0; y < surface_height(surface) ; y++) {
   			
      		d = surface_data(surface) + y*surface_stride(surface); 	
    	
	    	for (x = 0; x < surface_width(surface); x++)
	    	{
	    		uint32_t pixel = (s->data[y/8][x] & (1 << (y % 8)) )? rgb_to_pixel32(0x0,0x0,0x0) : rgb_to_pixel32(0xFF,0xFF,0xFF);
	    		((uint32_t *) d)[0] = pixel;
	    		d += surface_bytes_per_pixel(surface);
	    	}
	    	  	
    	}

    	//printf("%s: called 2 \n", __FUNCTION__);

        dpy_gfx_update(s->con, 0, 0, surface_width(surface) , surface_height(surface) );  
}

static void uc1601_invalidate_display(void * opaque)
{
    UC1601State *s = opaque;
    fprintf(stderr, "%s:  \n", __FUNCTION__);
    s->byteCount = 0;
}

static int vmstate_uc1601_post_load(void *opaque, int version_id)
{
    UC1601State *s = opaque;
    /* Make sure we redraw, and at the right size */
    uc1601_invalidate_display(s);
    return 0;
}

static const VMStateDescription vmstate_uc1601 = {
    .name = TYPE_UC1601,
    .version_id = 1,
    .minimum_version_id = 1,
    .post_load = vmstate_uc1601_post_load,
    .fields = (VMStateField[]) {
        VMSTATE_END_OF_LIST()
    }
};

static const GraphicHwOps uc1601_gfx_ops = {
    .invalidate  = uc1601_invalidate_display,
    .gfx_update  = uc1601_update_display,
};


static void uc1601_init(Object *obj)
{
   UC1601State *s =   UC1601(obj);
   DeviceState *dev = DEVICE(obj);

   s->isChipSelected = false;

   qdev_init_gpio_in(dev, uc1601_set_pin, 6);

   s->x = 0;
   s->y = 0;    
   s->bitPosition = 7;
   s->temp = 0;
   s->isNextCommandSetVBIASPotentiometer = false;
   s->isDisplayOn = false;
   s->byteCount = 0;

   //fprintf(stderr, "%s:  \n", __FUNCTION__);

}

static void uc1601_realize(DeviceState *dev, Error **errp)
{
    UC1601State *s = UC1601(dev);

    s->con = graphic_console_init(dev, 0, &uc1601_gfx_ops, s);

    debug_log_realize(UC1601_ERR_DEBUG, __func__);	
}

static void uc1601_reset(DeviceState *d)
{
    UC1601State *s = UC1601(d);

   qemu_console_resize(s->con, WIDTH*CZOOM, HEIGHT*CZOOM);

   debug_log_reset(UC1601_ERR_DEBUG, __func__);
}

static void uc1601_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    set_bit(DEVICE_CATEGORY_DISPLAY, dc->categories);
    dc->vmsd = &vmstate_uc1601;
    dc->desc  = "UltraChip 1601 LCD Display";
    dc->reset = uc1601_reset;
    dc->realize = uc1601_realize;

    //fprintf(stderr, "%s:  \n", __FUNCTION__);
}

static const TypeInfo uc1601_info = {
    .name          = TYPE_UC1601,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(UC1601State),
    .instance_init = uc1601_init,
    .class_init    = uc1601_class_init,
};


static void uc1601_register_types(void)
{
    type_register_static(&uc1601_info);
}

type_init(uc1601_register_types)
