/*
 * AT91 Reset Controller
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/

typedef struct AT91RSTCState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    MemoryRegion mmio;
    uint32_t cr;    // Control Register 
    uint32_t sr;    // Status Register
    uint32_t mr;    // Mode Register
} AT91RSTCState;

#define TYPE_AT91_RSTC "at91-rstc"
#define AT91_RSTC(obj) \
    OBJECT_CHECK(AT91RSTCState, (obj), TYPE_AT91_RSTC)

