/*
 * Lego NXT Mindstorms Board
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "qemu/osdep.h"
#include "qapi/error.h"
#include "qemu-common.h"
#include "cpu.h"
#include "hw/hw.h"
#include "hw/arm/arm.h"
#include "hw/sysbus.h"
#include "hw/devices.h"
#include "hw/boards.h"
#include "hw/block/flash.h"
#include "hw/display/uc1601.h"
#include "exec/address-spaces.h"
#include "at91_spi.h"
#include "at91_pio.h"
#include "at91_twi.h"
#include "at91_aic.h"
#include "at91_pmc.h"
#include "at91_intor.h"
#include "at91_rstc.h"
#include "at91_rtt.h"
#include "at91_pit.h"
#include "at91_mc.h"
#include "atmega48_twi.h"
#include "qemu/log.h"
#include "hw/loader.h"
#include "qemu/error-report.h"


#ifndef NXT_DEBUG
#define NXT_DEBUG 1
#endif


static void nxt_common_init(MemoryRegion *address_space_mem,
                                  MachineState *machine)
{
    ObjectClass *cpu_oc = NULL;
    Object *cpuobj = NULL;
    //ARMCPU *cpu = NULL;
    qemu_irq pic[32];
    qemu_irq pic1[32];
    DeviceState *dev = NULL;
    DeviceState *pioa = NULL;
    DeviceState *pmc = NULL;
    DeviceState *spi = NULL;
    DeviceState *twi = NULL;
    DeviceState *lcd = NULL;
    DeviceState *atmega48_twi = NULL;
    

    //machine->cpu_model = "arm7tdmi"; 
    machine->cpu_model = "arm926";    
    cpu_oc = cpu_class_by_name(TYPE_ARM_CPU, machine->cpu_model);
    if (!cpu_oc) {
        fprintf(stderr, "Unable to find CPU definition\n");
        exit(1);
    }   
    cpuobj = object_new(object_class_get_name(cpu_oc));
    object_property_set_bool(cpuobj, true, "realized", &error_fatal);
    //cpu = ARM_CPU(cpuobj);   

    // Flash and Ram is setup within the AT91 memory controller
    sysbus_create_simple(TYPE_AT91_MC, 0xFFFFFF00, NULL);

    dev = sysbus_create_varargs(TYPE_AT91_AIC, 0xFFFFF000,
                                qdev_get_gpio_in(DEVICE(cpuobj), ARM_CPU_IRQ),
                                qdev_get_gpio_in(DEVICE(cpuobj), ARM_CPU_FIQ),
                                NULL);

    for (int i = 0; i < 32; i++) {
        pic[i] = qdev_get_gpio_in(dev, i);
    }

    dev = sysbus_create_simple(TYPE_AT91_INTOR, -1, pic[1]);
    for (int i = 0; i < 32; i++) {
        pic1[i] = qdev_get_gpio_in(dev, i);
    }

    //sysbus_create_simple("at91,dbgu", 0xFFFFF200, pic1[0]);
    pmc = sysbus_create_simple(TYPE_AT91_PMC, 0xFFFFFC00, pic1[1]);
    sysbus_create_varargs(TYPE_AT91_RSTC, 0xFFFFFD00, NULL);
    pioa = sysbus_create_simple(TYPE_AT91_PIO, 0xFFFFF400, pic[2]);
    sysbus_create_simple(TYPE_AT91_RTT, 0xFFFFFD20, pic1[2]);
    sysbus_create_simple(TYPE_AT91_PIT, 0xFFFFFD30, pic1[3]);
    //sysbus_create_varargs("at91,tc", 0xFFFA0000, pic[12], pic[13], pic[14], NULL);
    spi = sysbus_create_simple(TYPE_AT91_SPI, 0xFFFE0000, pic[5]);
    twi = sysbus_create_simple(TYPE_AT91_TWI, 0xFFFB8000, pic[9]);
    atmega48_twi = sysbus_create_simple(TYPE_ATMEGA48_TWI, -1, NULL);
    lcd = sysbus_create_simple("uc1601", -1, NULL);
	
	// Connect AT91 TWI to PIO
	qdev_connect_gpio_out(twi, PIN_TWI_TWD, qdev_get_gpio_in(pioa, PIOA_PA3_TWD));
	qdev_connect_gpio_out(twi, PIN_TWI_TWCK, qdev_get_gpio_in(pioa, PIOA_PA4_TWCK));
	qdev_connect_gpio_out(pioa, PIOA_PA3_TWD, qdev_get_gpio_in(twi, PIN_TWI_TWD));
	qdev_connect_gpio_out(pioa, PIOA_PA4_TWCK, qdev_get_gpio_in(twi, PIN_TWI_TWCK));	

	// Connect atmega48 twi to PA3 and PA4
	qdev_connect_gpio_out(pioa, PA3, qdev_get_gpio_in(atmega48_twi, 0));
	qdev_connect_gpio_out(pioa, PA4, qdev_get_gpio_in(atmega48_twi, 1));
	qdev_connect_gpio_out(atmega48_twi, 0, qdev_get_gpio_in(pioa, PA3));
	qdev_connect_gpio_out(atmega48_twi, 1, qdev_get_gpio_in(pioa, PA4));

	// Connect SPI to PIO
	qdev_connect_gpio_out(spi, PIN_SPI_NPCS2, qdev_get_gpio_in(pioa, PIOA_PA10_NPCS2));				
	qdev_connect_gpio_out(spi, PIN_SPI_SPCK,  qdev_get_gpio_in(pioa, PIOA_PA14_SPCK));			
	qdev_connect_gpio_out(spi, PIN_SPI_MOSI,  qdev_get_gpio_in(pioa, PIOA_PA13_MOSI));			
	qdev_connect_gpio_out(spi, PIN_SPI_MISO,  qdev_get_gpio_in(pioa, PIOA_PA12_MISO));
	
	// Connect LCD uc1601 driver to PA10, PA12, PA13 and PA14   		
	qdev_connect_gpio_out(pioa, PA10, qdev_get_gpio_in(lcd, PIN_CS0));		
	qdev_connect_gpio_out(pioa, PA12, qdev_get_gpio_in(lcd, PIN_CD));
	qdev_connect_gpio_out(pioa, PA14, qdev_get_gpio_in(lcd, PIN_SCK));
	qdev_connect_gpio_out(pioa, PA13, qdev_get_gpio_in(lcd, PIN_SDA));
	
    // Connect PMC to PIO
    qdev_connect_gpio_out(pmc, PIN_AT91_PMC_PCK0, qdev_get_gpio_in(pioa, PIOA_PA6_PCK0));  

    if (!machine->kernel_filename) {
        fprintf(stderr, "Guest image must be specified (using -kernel)\n");
        exit(1);
    }

    if (machine->kernel_filename) {

        int image_size = load_image_targphys(machine->kernel_filename, 0x0, AT91_FLASH_SIZE);
        
        if (image_size < 0) {
            error_report("Could not load kernel '%s'", machine->kernel_filename);
            exit(1);
        }
    }
}

static void nxt_init(MachineState *machine)
{
    nxt_common_init(get_system_memory(), machine);
}

static void nxt_machine_init(MachineClass *mc)
{
    mc->desc = "Lego NXT Mindstorms";
    mc->init = nxt_init;
}

DEFINE_MACHINE("nxt", nxt_machine_init)
