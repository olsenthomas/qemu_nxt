/*
 * AT91 two-wire interface (I2C)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#define PIN_TWI_TWD 0
#define PIN_TWI_TWCK 1

typedef struct AT91TWIState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    MemoryRegion mmio;
    qemu_irq parent_irq;
    qemu_irq out[2];
    uint32_t cr;
    uint32_t mmr;
    uint32_t iadr;
    uint32_t cwgr;
    uint32_t sr;
    uint32_t ier;
    uint32_t idr;
    uint32_t imr;
    uint32_t rhr;
    uint32_t thr;        
} AT91TWIState;

#define TYPE_AT91_TWI "at91-twi"
#define AT91_TWI(obj) \
    OBJECT_CHECK(AT91TWIState, (obj), TYPE_AT91_TWI)

const char* at91_twi_AddrToRegister(hwaddr addr);
