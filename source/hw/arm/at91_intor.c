/*
 * AT91 Interrupt Logic OR
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "at91_intor.h"

static void at91_intor_set_irq(void *opaque, int irq, int level)
{
    AT91IntOrState *s = opaque;

    if (level) {
        s->sources |= 1 << irq;
    } else {
        s->sources &= ~(1 << irq);
    }
    qemu_set_irq(s->parent_irq, !!s->sources);
}

static void at91_intor_reset(DeviceState *dev)
{
    AT91IntOrState *s = AT91_INTOR(dev);    
    s->sources = 0;
}

static void at91_intor_init(Object *obj)
{
    AT91IntOrState *s = AT91_INTOR(obj);
    DeviceState *dev = DEVICE(obj);
    qdev_init_gpio_in(dev, at91_intor_set_irq, 32);
    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->parent_irq);
}


static void at91_intor_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset = at91_intor_reset;
//    dc->vmsd = &vmstate_at91_intor;
}

static const TypeInfo at91_intor_info = {
    .name          = TYPE_AT91_INTOR,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(AT91IntOrState),
    .instance_init = at91_intor_init,
    .class_init    = at91_intor_class_init,
};

static void at91_intor_register_types(void)
{
    type_register_static(&at91_intor_info);
}

type_init(at91_intor_register_types)
