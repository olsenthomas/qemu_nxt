/*
 * AT91 Real-time Timer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/
 

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "qemu/timer.h"
#include "qemu/main-loop.h"
#include "at91_rtt.h"

#define RTT_SIZE        0x10 /* 16 Bytes/4 registers */

#define RTT_MR          0x00 /* Mode Register */
#define RTT_AR          0x04 /* Alarm Register */
#define RTT_VR          0x08 /* Value Register */
#define RTT_SR          0x0c /* Status Register */

#define MR_ALMIEN       0x10000
#define MR_RTTINCIEN    0x20000
#define MR_RTTRST       0x40000

#define SR_ALMS         0x01
#define SR_RTTINC       0x02

static void at91_rtt_tick(void *opaque)
{
    AT91RTTState *s = opaque;

    s->vr++;
    s->sr |= SR_RTTINC;
    if (s->ar != ~0 && s->vr == s->ar + 1) {
        s->sr |= SR_ALMS;
    }
    if (((s->sr & SR_RTTINC) && (s->mr & MR_RTTINCIEN)) ||
        ((s->sr & SR_ALMS) && (s->mr & MR_ALMIEN))) {
        qemu_set_irq(s->irq, 1);
    }
}

static uint64_t at91_rtt_mem_read(void *opaque, hwaddr addr, unsigned int size)
{
    AT91RTTState *s = opaque;
    uint32_t sr;

    switch (addr) {
    case RTT_MR:
        return s->mr;
    case RTT_AR:
        return s->ar;
    case RTT_VR:
        return s->vr;
    case RTT_SR:
        sr = s->sr;
        qemu_set_irq(s->irq, 0);
        s->sr = 0;
        return sr;
    default:
        return 0;
    }
}

static void at91_rtt_mem_write(void *opaque, hwaddr addr, uint64_t val64, unsigned int size)
{
    AT91RTTState *s = opaque;
    uint32_t value = val64;

    switch (addr) {
    case RTT_MR:
        if (value & MR_RTTRST) {
            s->vr = 0;
            if ((value & 0xffff) == 0) {
                ptimer_set_freq(s->timer, 1);
            } else {
                ptimer_set_freq(s->timer, 0x8000 / (value & 0xffff));
            }
        }
        s->mr = value;
        break;
    case RTT_AR:
        s->ar = value;
        break;
    }
}

static void at91_rtt_reset(DeviceState *dev)
{
    AT91RTTState *s = AT91_RTT(dev);

    s->mr = 0x8000;
    s->ar = ~0;
    s->vr = 0;
    s->sr = 0;
}

static const MemoryRegionOps at91_rtt_ops = {
    .read = at91_rtt_mem_read,
    .write = at91_rtt_mem_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_at91_rtt = {
    .name = TYPE_AT91_RTT,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32(mr,AT91RTTState),
        VMSTATE_UINT32(ar,AT91RTTState),
        VMSTATE_UINT32(vr,AT91RTTState),
        VMSTATE_UINT32(sr,AT91RTTState), 
        VMSTATE_END_OF_LIST()
    }
};

static void at91_rtt_init(Object *obj)
{
    AT91RTTState *s = AT91_RTT(obj);

    QEMUBH *bh = qemu_bh_new(at91_rtt_tick, s);
    s->timer = ptimer_init(bh, PTIMER_POLICY_DEFAULT);
    ptimer_set_freq(s->timer, 1);
    ptimer_set_limit(s->timer, 1, 1);
    ptimer_run(s->timer, 0);

    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->irq);

    memory_region_init_io(&s->mmio, obj, &at91_rtt_ops, s, TYPE_AT91_RTT, RTT_SIZE);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);  

    
}


static void at91_rtt_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset = at91_rtt_reset;
    dc->vmsd = &vmstate_at91_rtt;
}

static const TypeInfo at91_rtt_info = {
    .name          = TYPE_AT91_RTT,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(AT91RTTState),
    .instance_init = at91_rtt_init,
    .class_init    = at91_rtt_class_init,
};

static void at91_rtt_register_types(void)
{
    type_register_static(&at91_rtt_info);
}

type_init(at91_rtt_register_types)
