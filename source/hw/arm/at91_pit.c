/*
 * AT91 Periodic Interval Timer Controller
 *
 * Copyright (c) 2009 Filip Navara, 2016 Thomas Olsen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "qemu/timer.h"
#include "hw/ptimer.h"
#include "qemu/main-loop.h"
#include "at91.h"
#include "at91_pit.h"
#include "debug.h"

#define PIT_SIZE        0x10 /* 16 Bytes/4 registers */

#define PIT_MR          0x00 /* Mode Register */
#define PIT_SR          0x04 /* Status Register */
#define PIT_PIVR        0x08 /* Periodic Interval Value Register */
#define PIT_PIIR        0x0c /* Periodic Interval Image Register */

#define PIT_LIMIT(s) \
    (((s)->mr & 0xfffff) + 1)

#ifndef AT91_PITC_ERR_DEBUG
#define AT91_PITC_ERR_DEBUG 0
#endif

static void at91_pit_tick(void *opaque)
{
    AT91PITState *s = opaque;

    s->sr |= 1;
    s->picnt++;
    if (s->mr & 0x2000000) {
        qemu_set_irq(s->irq, 1);
    }
}

static uint64_t at91_pit_mem_read(void *opaque, hwaddr addr, unsigned int size)
{
    AT91PITState *s = opaque;

    debug_log_mem_read(AT91_PITC_ERR_DEBUG, __func__, addr, "", size);

    switch (addr) {
    case PIT_MR:
        return s->mr;
    case PIT_SR:
        return s->sr;
    case PIT_PIVR:
        s->sr = 0;
        qemu_set_irq(s->irq, 0);
        /* Fall-through */
    case PIT_PIIR:
        return
            ((PIT_LIMIT(s) - ptimer_get_count(s->timer)) & 0xfffff) |
            (s->picnt << 20);

    default:
        return 0;
    }
}

static void at91_pit_mem_write(void *opaque, hwaddr addr, uint64_t val64, unsigned int size)
{
    AT91PITState *s = opaque;
    uint32_t value = val64;

    debug_log_mem_write(AT91_PITC_ERR_DEBUG, __func__, addr, "", val64, size);

    if (addr == PIT_MR) {
        s->mr = value;
        if (value & 0x1000000) {
            ptimer_set_freq(s->timer, at91_master_clock_frequency / 16);
            ptimer_set_limit(s->timer, PIT_LIMIT(s), 1);
            ptimer_run(s->timer, 0);
        } else {
            ptimer_stop(s->timer);
        }
    }
}

static const MemoryRegionOps at91_pit_ops = {
    .read = at91_pit_mem_read,
    .write = at91_pit_mem_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_at91_pit = {
    .name = TYPE_AT91_PIT,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32(mr,AT91PITState),
        VMSTATE_UINT32(sr,AT91PITState),
        VMSTATE_UINT32(pvir,AT91PITState),
        VMSTATE_UINT32(piir,AT91PITState), 
        VMSTATE_END_OF_LIST()
    }
};


static void at91_pit_reset(DeviceState *dev)
{
    AT91PITState *s = AT91_PIT(dev);

    debug_log_reset(AT91_PITC_ERR_DEBUG, __func__);

    s->mr = 0xfffff;
    s->sr = 0;
    s->picnt = 0;
    ptimer_stop(s->timer);
}

static void at91_pit_init(Object *obj)
{
    AT91PITState *s = AT91_PIT(obj);
    QEMUBH *pit_bh;

    pit_bh = qemu_bh_new(at91_pit_tick, s);
    s->timer = ptimer_init(pit_bh, PTIMER_POLICY_DEFAULT);

    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->irq);

    memory_region_init_io(&s->mmio, obj, &at91_pit_ops, s, TYPE_AT91_PIT, PIT_SIZE);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);  
      
}

static void at91_pit_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset = at91_pit_reset;
    dc->vmsd = &vmstate_at91_pit;
}

static const TypeInfo at91_pit_info = {
    .name          = TYPE_AT91_PIT,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(AT91PITState),
    .instance_init = at91_pit_init,
    .class_init    = at91_pit_class_init,
};

static void at91_pit_register_types(void)
{
    type_register_static(&at91_pit_info);
}

type_init(at91_pit_register_types)
