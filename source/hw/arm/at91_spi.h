/*
 * AT91 SPI
 *
 * Copyright (c) 2010 Thomas Olsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
 // Output pins that simulate the SPCK, MOSI, and NPCS on the NXT board SPI to support 4 slaves
#define PIN_SPI_SPCK	0
#define PIN_SPI_MOSI	1
#define PIN_SPI_NPCS0	2
#define PIN_SPI_NPCS1	3
#define PIN_SPI_NPCS2	4
#define PIN_SPI_NPCS3	5
#define PIN_SPI_MISO    6

const char* offsetToRegister(uint64_t offset);

#define SPI_CR 		0x00
#define SPI_MR		0x04
#define SPI_RDR		0x08
#define SPI_TDR		0x0C
#define SPI_SR		0x10
#define SPI_IER		0x14
#define SPI_IDR		0x18	
#define SPI_IMR		0x1C
#define SPI_CSR0	0x30
#define SPI_CSR1	0x34
#define SPI_CSR2	0x38
#define SPI_CSR3	0x3C
#define SPI_TPR		0x108
#define SPI_TCR		0x10C
#define SPI_TNPR	0x118
#define SPI_TNCR	0x11C		
#define SPI_PTCR	0x120
#define SPI_PTSR	0x124
#define SPI_SIZE	0x4000


// SPI Control Register
#define SPI_SPIEN       ((unsigned int) 0x1 <<  0) 
#define SPI_SPIDIS      ((unsigned int) 0x1 <<  1) 
#define SPI_SWRST       ((unsigned int) 0x1 <<  7) 
#define SPI_LASTXFER    ((unsigned int) 0x1 << 24) 
// SPI Status Register
#define SPI_RDRF        ((unsigned int) 0x1 <<  0) 
#define SPI_TDRE        ((unsigned int) 0x1 <<  1) 
#define SPI_MODF        ((unsigned int) 0x1 <<  2) 
#define SPI_OVRES       ((unsigned int) 0x1 <<  3) 
#define SPI_ENDRX       ((unsigned int) 0x1 <<  4) 
#define SPI_ENDTX       ((unsigned int) 0x1 <<  5) 
#define SPI_RXBUFF      ((unsigned int) 0x1 <<  6) 
#define SPI_TXBUFE      ((unsigned int) 0x1 <<  7) 
#define SPI_NSSR        ((unsigned int) 0x1 <<  8) 
#define SPI_TXEMPTY     ((unsigned int) 0x1 <<  9) 
#define SPI_SPIENS      ((unsigned int) 0x1 << 16) 
// SPI Mode Register
#define SPI_MSTR        ((unsigned int) 0x1 <<  0) 
#define SPI_PS          ((unsigned int) 0x1 <<  1) 
#define SPI_PCSDEC      ((unsigned int) 0x1 <<  2) 
#define SPI_FDIV        ((unsigned int) 0x1 <<  3) 
#define SPI_MODFDIS     ((unsigned int) 0x1 <<  4) 
#define SPI_LLB         ((unsigned int) 0x1 <<  7) 
#define SPI_PCS         ((unsigned int) 0xF << 16) 
#define SPI_DLYBCS      ((unsigned int) 0xFF << 24) 
// SPI Transmit Data Register
#define SPI_TD          ((unsigned int) 0xFFFF <<  0) 
#define SPI_TPCS        ((unsigned int) 0xF << 16) 
// SPI Control Status Register
#define SPI_CPOL        ((unsigned int) 0x1 <<  0) 
#define SPI_NCPHA       ((unsigned int) 0x1 <<  1) 
#define SPI_CSAAT       ((unsigned int) 0x1 <<  3) 
#define SPI_BITS        ((unsigned int) 0xF <<  4) 
#define SPI_BITS_8      ((unsigned int) 0x0 <<  4) 
#define SPI_BITS_9      ((unsigned int) 0x1 <<  4) 
#define SPI_BITS_10     ((unsigned int) 0x2 <<  4) 
#define SPI_BITS_11     ((unsigned int) 0x3 <<  4) 
#define SPI_BITS_12     ((unsigned int) 0x4 <<  4)
#define SPI_BITS_13     ((unsigned int) 0x5 <<  4) 
#define SPI_BITS_14     ((unsigned int) 0x6 <<  4) 
#define SPI_BITS_15     ((unsigned int) 0x7 <<  4) 
#define SPI_BITS_16     ((unsigned int) 0x8 <<  4) 
#define SPI_SCBR        ((unsigned int) 0xFF <<  8) 
#define SPI_DLYBS       ((unsigned int) 0xFF << 16) 
#define SPI_DLYBCT      ((unsigned int) 0xFF << 24) 
// PDC
#define PDC_RXTEN       ((unsigned int) 0x1 <<  0) 
#define PDC_RXTDIS      ((unsigned int) 0x1 <<  1) 
#define PDC_TXTEN       ((unsigned int) 0x1 <<  8) 
#define PDC_TXTDIS      ((unsigned int) 0x1 <<  9) 


typedef struct AT91SPIState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    MemoryRegion mmio;
    qemu_irq parent_irq;    
    qemu_irq out[7]; 
    uint32_t CR;
    uint32_t MR;	
    uint32_t RDR;
    uint32_t TDR;
    uint32_t SR;
    uint32_t IER;
    uint32_t IDR;
    uint32_t IMR;
    uint32_t CSR0;
    uint32_t CSR1;
    uint32_t CSR2;
    uint32_t CSR3;
    uint32_t shiftRegister; // Internal shift register used by MISO and MOSI
    bool isShiftRegisterEmpty;
    bool isTDREmpty;
    bool isSoftwareReset;
    bool isSPIEnabled;

    // PDC part
    
    uint32_t TPR;
    uint32_t TCR;
    uint32_t TNPR;
    uint32_t TNCR;
    uint32_t PTCR;
    uint32_t PTSR;
    bool isTransmitterTransferEnabled;   
    bool isReceiverTransferEnabled;
   
} AT91SPIState;

#define TYPE_AT91_SPI          "at91-spi"
#define AT91_SPI(obj) \
    OBJECT_CHECK(AT91SPIState, (obj), TYPE_AT91_SPI)







 
