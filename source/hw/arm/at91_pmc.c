/*
 * AT91 Power Management Controller
 *
 * Copyright (c) 2009 Filip Navara
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/
 

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "qemu/timer.h"
#include "qemu/log.h"
#include "at91.h"
#include "at91_pmc.h"
#include "debug.h"

#define PMC_SIZE        0x100 /* 256 Bytes/64 registers */

#define PMC_SCER        0x00 /* System Clock Enable Register */
#define PMC_SCDR        0x04 /* System Clock Disable Register */
#define PMC_SCSR        0x08 /* System Clock Status Register */
#define PMC_PCER        0x10 /* Peripheral Clock Enable Register */
#define PMC_PCDR        0x14 /* Peripheral Clock Disable Register */
#define PMC_PCSR        0x18 /* Peripheral Clock Status Register */
#define PMC_MOR         0x20 /* Main Oscillator Register */
#define PMC_MCFR        0x24 /* Main Clock Frequency Register */
#define PMC_PLLA        0x28 /* PLL A Register */
#define PMC_PLLB        0x2c /* PLL B Register */
#define PMC_MCKR        0x30 /* Master Clock Register */
#define PMC_PCK0        0x40 /* Programmable Clock Register 1*/
#define PMC_PCK1        0x44 /* Programmable Clock Register 2*/
#define PMC_PCK2        0x48 /* Programmable Clock Register 3*/
#define PMC_IER         0x60 /* Interrupt Enable Register */
#define PMC_IDR         0x64 /* Interrupt Disable Register */
#define PMC_IMR         0x6c /* Interrupt Mask Register */
#define PMC_SR          0x68 /* Status Register */

#define SR_MOSCS        0x01
#define SR_LOCKA        0x02
#define SR_LOCKB        0x04
#define SR_MCKRDY       0x08
#define SR_PCK0RDY      0x100
#define SR_PCK1RDY      0x200
#define SR_PCK2RDY      0x400
#define SR_PCK3RDY      0x800

#define SO_FREQ         32768
#define MO_FREQ         9216000

#ifndef AT91_PMC_ERR_DEBUG
#define AT91_PMC_ERR_DEBUG 0
#endif

int at91_master_clock_frequency = SO_FREQ;

static void at91_pmc_update_irq(AT91PMCState *s)
{
    qemu_set_irq(s->parent_irq, !!(s->sr & s->imr));
}

static void at91_update_master_clock(AT91PMCState *s)
{
    int mck_freq = MO_FREQ;

    /* Clock selection */
    switch (s->mckr & 3) {
    case 0: /* Slow */
        mck_freq = SO_FREQ;
        break;
    case 1: /* Main */
        if (!(s->sr & SR_MOSCS))
            mck_freq = 0;
        break;
    case 2: /* PLL A */
        if ((s->plla & 0xff) != 0 &&
            (s->plla & 0x3ff80) != 0) {
            mck_freq /= s->plla & 0xff;
            mck_freq *= ((s->plla >> 16) & 0x7ff) + 1;
        } else {
            mck_freq = 0;
        }
        break;
    case 3: /* PLL B */
        if ((s->pllb & 0xff) != 0 &&
            (s->pllb & 0x3ff80) != 0) {
            mck_freq /= s->pllb & 0xff;
            mck_freq *= ((s->pllb >> 16) & 0x7ff) + 1;
        } else {
            mck_freq = 0;
        }
        break;
    }

    if (mck_freq != 0) {
        mck_freq /= 1 << ((s->mckr >> 2) & 7);
        mck_freq /= 1 << ((s->mckr >> 8) & 3);
        s->mck_freq = mck_freq;
        at91_master_clock_frequency = mck_freq;
        s->sr |= SR_MCKRDY;
    } else {
        s->sr &= ~SR_MCKRDY;
    }
}

static uint64_t at91_pmc_mem_read(void *opaque, hwaddr addr, unsigned int size)
{
    AT91PMCState *s = opaque;

    debug_log_mem_read(AT91_PMC_ERR_DEBUG, __func__, addr, "", size);

    switch (addr) {
    case PMC_SCSR:
        return s->scsr;
    case PMC_PCSR:
        return s->pcsr;
    case PMC_MOR:
        return s->mor;
    case PMC_MCFR:
        if (s->mor & 1)
            return (1 << 16) | (MO_FREQ / SO_FREQ / 16);
        return 0;
    case PMC_PCK0:
        return s->pckr[0];
    case PMC_PCK1:
        return s->pckr[1];
    case PMC_PCK2:
        return s->pckr[2];
    case PMC_SR:
        return s->sr;
    case PMC_IMR:
        return s->imr;
    default:
        return 0;
    }
}

static void at91_pmc_mem_write(void *opaque, hwaddr addr, uint64_t val64, unsigned int size)
{
    AT91PMCState *s = opaque;
    uint32_t value = val64;
   
    debug_log_mem_write(AT91_PMC_ERR_DEBUG, __func__, addr, "", val64, size);

    switch (addr) {
    case PMC_SCER:
        s->scsr |= value & 0xf80;
        break;
    case PMC_SCDR:
        s->scsr &= ~(value & 0xf80);
        break;
    case PMC_PCER:
        s->pcsr |= value & ~3;
        break;
    case PMC_PCDR:
        s->pcsr &= ~(value & ~3);
        break;
    case PMC_MOR:
        /* Main Oscillator bypassing is not supported, so first two
           bits are ignored. Bits 8-15 specify the OSCOUNT, which is
           also currently ignored. */
        s->mor = value;
        s->sr |= SR_MOSCS;
        break;
    case PMC_PLLA:
        s->plla = value;
        /* OUTA, PLLACOUNT ignored for now */
        s->sr |= SR_LOCKA;
        break;
    case PMC_PLLB:
        s->pllb = value;
        /* OUTB, PLLBCOUNT ignored for now */
        s->sr |= SR_LOCKB;
        break;
    case PMC_MCKR:
        s->mckr = value;
        break;
    case PMC_PCK0:
        s->pckr[0] = value;
        break;
    case PMC_PCK1:
        s->pckr[1] = value;
        break;
    case PMC_PCK2:
        s->pckr[2] = value;
        break;
    case PMC_IER:
        s->imr |= value;
        break;
    case PMC_IDR:
        s->imr &= ~value;
        break;
    default:
        return;
    }

    at91_update_master_clock(s);
    at91_pmc_update_irq(s);
}

static const MemoryRegionOps at91_pmc_ops = {
    .read = at91_pmc_mem_read,
    .write = at91_pmc_mem_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_at91_pmc = {
    .name = TYPE_AT91_PMC,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
    VMSTATE_UINT32(scer,AT91PMCState),
    VMSTATE_UINT32(scdr,AT91PMCState),
    VMSTATE_UINT32(scsr,AT91PMCState),
    VMSTATE_UINT32(pcer,AT91PMCState),
    VMSTATE_UINT32(pcdr,AT91PMCState),
    VMSTATE_UINT32(pcsr,AT91PMCState),
    VMSTATE_UINT32(mor,AT91PMCState),
    VMSTATE_UINT32(mcfr,AT91PMCState),
    VMSTATE_UINT32(plla,AT91PMCState),
    VMSTATE_UINT32(pllb,AT91PMCState),
    VMSTATE_UINT32(mckr,AT91PMCState),
    VMSTATE_UINT32_ARRAY(pckr,AT91PMCState, 3),
    VMSTATE_UINT32(ier,AT91PMCState),
    VMSTATE_UINT32(idr,AT91PMCState), 
    VMSTATE_UINT32(imr,AT91PMCState),
    VMSTATE_UINT32(sr,AT91PMCState),
    VMSTATE_END_OF_LIST()
    }
};

static void at91_pmc_reset(DeviceState *dev)
{
    AT91PMCState *s = AT91_PMC(dev); 

    debug_log_reset(AT91_PMC_ERR_DEBUG, __func__);

    s->scsr = 1;
    s->pcsr = 0;
    s->mor = 0;
    s->plla = s->pllb = 0x3f00;
    s->mckr = 0;
    s->pckr[0] = s->pckr[1] = s->pckr[2] = 0;
    s->sr = 8;
    s->imr = 0;
    s->mck_freq = SO_FREQ;
}

static void at91_pmc_init(Object *obj)
{
    AT91PMCState *s = AT91_PMC(obj);    

    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->parent_irq);
    qdev_init_gpio_out(DEVICE(obj), s->out, 7);

    memory_region_init_io(&s->mmio, obj, &at91_pmc_ops, s, TYPE_AT91_PMC, PMC_SIZE);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);  

}


static void at91_pmc_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset = at91_pmc_reset;
    dc->vmsd = &vmstate_at91_pmc;
    dc->desc = "Power Manager Controller";
}

static const TypeInfo at91_pmc_info = {
    .name          = TYPE_AT91_PMC,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(AT91PMCState),
    .instance_init = at91_pmc_init,
    .class_init    = at91_pmc_class_init,
};

static void at91_pmc_register_types(void)
{
    type_register_static(&at91_pmc_info);
}

type_init(at91_pmc_register_types)