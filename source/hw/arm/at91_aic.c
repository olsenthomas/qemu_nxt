/*
 * AT91 Advanced Interrupt Controller.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "qemu/log.h"
#include "at91_aic.h"
#include "debug.h"

/* TODO: Inverting external sources based on SRCTYPE in SMR register */

#define AIC_SIZE        0x200

#define AIC_SMR         0x000 /* Source Mode Register */
#define AIC_SVR         0x080 /* Source Vector Register */
#define AIC_IVR         0x100 /* IRQ Vector Register */
#define AIC_FVR         0x104 /* FIQ Vector Register */
#define AIC_ISR         0x108 /* Interrupt Status Register */
#define AIC_IPR         0x10c /* Interrupt Pending Register */
#define AIC_IMR         0x110 /* Interrupt Mask Register */
#define AIC_CISR        0x114 /* Core Interrupt Status Register */
#define AIC_IECR        0x120 /* Interrupt Enable Command Register */
#define AIC_IDCR        0x124 /* Interrupt Disable Command Register */
#define AIC_ICCR        0x128 /* Interrupt Clear Command Register */
#define AIC_ISCR        0x12c /* Interrupt Set Command Register */
#define AIC_EOICR       0x130 /* End of Interrupt Command Register */
#define AIC_SPU         0x134 /* Spurious Vector Register */
#define AIC_DCR         0x138 /* Debug Control Register (Protect) */
#define AIC_FFER        0x140 /* Fast Forcing Enable Register */
#define AIC_FFDR        0x144 /* Fast Forcing Disable Register */
#define AIC_FFSR        0x148 /* Fast Forcing Status Register */

#define DCR_PROT        0x01 /* Protect Mode Enabled */
#define DCR_GMSK        0x02 /* General Mask */

#define AIC_GET_PRIORITY(s, irq) \
    ((int)((s)->smr[(irq)] & 7))
#define AIC_IS_EDGE_TRIGGERED(s, irq) \
    ((int)((s)->smr[(irq)] & (1 << 5)))

#ifndef AT91_AIC_ERR_DEBUG
#define AT91_AIC_ERR_DEBUG 0
#endif


static uint8_t at91_aic_highest(struct AT91AICState *s, uint8_t *priority)
{
    uint32_t pending_interrupts;
    uint8_t highest_interrupt = 0;
    uint8_t highest_priority = 0;
    int i;

    pending_interrupts = s->ipr & s->imr & ~s->ffsr;
    for (i = 31; i >= 1; i--) {
        if (pending_interrupts & (1 << i)) {
            if (AIC_GET_PRIORITY(s, i) >= highest_priority) {
                highest_interrupt = i;
                highest_priority = AIC_GET_PRIORITY(s, i);
            }
        }
    }

    *priority = highest_priority;
    return highest_interrupt;
}

static void at91_aic_update(AT91AICState *s)
{
    uint32_t requests = s->ipr & s->imr;
    uint32_t fiq_mask = 1 | s->ffsr;

    if (s->dcr & DCR_GMSK) {
        s->cisr = 0;
    } else {
        /* Fast interrupts */
        s->cisr = !!(requests & fiq_mask);
        /* Priority-driven normal interrupts */
        if (requests & ~fiq_mask) {
            uint8_t highest_priority;
            at91_aic_highest(s, &highest_priority);
            if (s->stack_pos < 0 ||
                s->stack_pri[s->stack_pos] < highest_priority) {
                s->cisr |= 2;
            }
        }
    }

    qemu_set_irq(s->parent_fiq, s->cisr & 1);
    qemu_set_irq(s->parent_irq, s->cisr & 2);
}

static void at91_aic_set_irq(void *opaque, int irq, int level)
{
    struct AT91AICState *s = (struct AT91AICState *) opaque;
    int mask = 1 << irq;

    /* TODO: External egde-triggering */
    if (level)
        s->ipr |= mask;
    else if (!AIC_IS_EDGE_TRIGGERED(s, irq))
        s->ipr &= ~mask;

    at91_aic_update(s);
}

static inline void at91_aic_irq_enter(AT91AICState *s, uint8_t irq, uint8_t priority)
{
    if (s->stack_pos < 7 && irq != 0) {
        s->stack_pos++;
        s->stack_irq[s->stack_pos] = irq;
        s->stack_pri[s->stack_pos] = priority;

        if (AIC_IS_EDGE_TRIGGERED(s, irq)) {
            s->ipr &= ~(1 << irq);
            at91_aic_update(s);
        }
    }
}

static uint64_t at91_aic_mem_read(void *opaque, hwaddr addr, unsigned int size)
{
    AT91AICState *s = opaque;
    uint8_t current_irq;
    uint8_t current_pri;

    debug_log_mem_read(AT91_AIC_ERR_DEBUG, __func__, addr, "", size);

    switch (addr) {
    case AIC_IVR: /* Interrupt vector register */
        current_irq = at91_aic_highest(s, &current_pri);
        if (!(s->dcr & DCR_PROT)) {
            at91_aic_irq_enter(s, current_irq, current_pri);
        }
        return current_irq == 0 ? s->spu : s->svr[current_irq];
    case AIC_FVR: /* FIQ vector register */
        if (s->ipr & 1) {
            s->ipr &= ~1;
            at91_aic_update(s);
            return s->svr[0];
        } else if (s->ipr & s->ffsr) {
            return s->svr[0];
        }
        return s->spu;
    case AIC_ISR: /* Interrupt status register */
        if (s->stack_pos < 0)
            return 0;
        return s->stack_irq[s->stack_pos];
    case AIC_IPR: /* Interrupt pending register */
        return s->ipr;
    case AIC_IMR: /* Interrupt mask register */
        return s->imr;
    case AIC_CISR: /* Core interrupt status register */
        return s->cisr;
    case AIC_SPU: /* Spurious interrupt vector register */
        return s->spu;
    case AIC_DCR:
        return s->dcr;
    case AIC_FFSR:
        return s->ffsr;
    case AIC_SMR ... AIC_SMR + 127:
        return s->smr[(addr - AIC_SMR) >> 2];
    case AIC_SVR ... AIC_SVR + 127:
        return s->svr[(addr - AIC_SVR) >> 2];
    default:
        return 0;
    }
}

static void at91_aic_mem_write(void *opaque, hwaddr addr,
                                uint64_t val64, unsigned int size)
{
    AT91AICState *s = opaque;
    uint32_t value = val64; 
    uint8_t current_irq;
    uint8_t current_pri;
    int irq;

    debug_log_mem_write(AT91_AIC_ERR_DEBUG, __func__, addr, "", val64, size);

    switch (addr) {
    case AIC_IVR:
        if (s->dcr & DCR_PROT) {
            current_irq = at91_aic_highest(s, &current_pri);
            at91_aic_irq_enter(s, current_irq, current_pri);
        }
        break;
    case AIC_IECR:
        s->imr |= value;
        break;
    case AIC_IDCR:
        s->imr &= ~value;
        break;
    case AIC_ICCR:
        for (irq = 0; irq < 32; irq++) {
            if (!AIC_IS_EDGE_TRIGGERED(s, irq))
                value &= ~(1 << irq);
        }
        s->ipr &= value;
        break;
    case AIC_ISCR:
        for (irq = 0; irq < 32; irq++) {
            if (!AIC_IS_EDGE_TRIGGERED(s, irq))
                value &= ~(1 << irq);
        }
        s->ipr |= value;
        break;
    case AIC_EOICR: /* End of interrupt */
        if (s->stack_pos >= 0)
            s->stack_pos--;
        break;
    case AIC_SPU:
        s->spu = value;
        return;
    case AIC_DCR:
        s->dcr = value;
        break;
    case AIC_FFER:
        s->ffsr |= value;
        break;
    case AIC_FFDR:
        s->ffsr &= ~value;
        break;
    case AIC_SMR ... AIC_SMR + 127:
        s->smr[(addr - AIC_SMR) >> 2] = value;
        break;
    case AIC_SVR ... AIC_SVR + 127:
        s->svr[(addr - AIC_SVR) >> 2] = value;
        return;
    default:
        return;
    }

    at91_aic_update(s);
}

static const MemoryRegionOps at91_aic_ops = {
    .read = at91_aic_mem_read,
    .write = at91_aic_mem_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_at91_aic = {
    .name = TYPE_AT91_AIC,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
    VMSTATE_UINT32_ARRAY(smr, AT91AICState, 32),
    VMSTATE_UINT32_ARRAY(svr, AT91AICState, 32),    
    VMSTATE_END_OF_LIST()
    }
};


static void at91_aic_reset(DeviceState *dev)
{
    AT91AICState *s = AT91_AIC(dev);
    int i;

    debug_log_reset(AT91_AIC_ERR_DEBUG, __func__);
    
    for (i = 0; i < 32; i++) {
        s->smr[i] = 0;
        s->svr[i] = 0;
    }
    s->ipr = 0;
    s->imr = 0;
    s->cisr = 0;
    s->spu = 0;
    s->dcr = 0;
    s->ffsr = 0;
    s->stack_pos = -1;
}

static void at91_aic_init(Object *obj)
{
    AT91AICState *s     = AT91_AIC(obj);

    qdev_init_gpio_in(DEVICE(obj), at91_aic_set_irq, 32);
    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->parent_irq);
    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->parent_fiq);

    memory_region_init_io(&s->mmio, obj, &at91_aic_ops, s, TYPE_AT91_AIC, AIC_SIZE);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);

}

static void at91_aic_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset = at91_aic_reset;
    dc->vmsd = &vmstate_at91_aic;
}

static const TypeInfo at91_aic_info = {
    .name          = TYPE_AT91_AIC,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(AT91AICState),
    .instance_init = at91_aic_init,
    .class_init    = at91_aic_class_init,
};

static void at91_aic_register_types(void)
{
    type_register_static(&at91_aic_info);
}

type_init(at91_aic_register_types)