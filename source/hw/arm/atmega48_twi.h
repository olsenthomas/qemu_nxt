/*
 * ATmega48 two-wire interface
 *
 * Copyright (c) 2016 Thomas Olsen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/


typedef struct ATmega48TWIState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    qemu_irq out[2];    
} ATmega48TWIState;

#define TYPE_ATMEGA48_TWI "atmega48-twi"
#define ATMEGA48_PIT(obj) \
    OBJECT_CHECK(ATmega48TWIState, (obj), TYPE_ATMEGA48_TWI)

