/*
 * AT91 Reset Controller
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "qemu/log.h"
#include "at91_rstc.h"
#include "debug.h"

/* #define DEBUG_RSTC */

#define RSTC_SIZE        0x10

#define RSTC_CR          0x00 /* Control Register */
#define RSTC_SR          0x04 /* Status Register */
#define RSTC_MR          0x08 /* Mode Register */

#define WRITE_KEY        0xa5

#define CR_PROCRST       0x01 /* Processor Reset */
#define CR_PERRST        0x04 /* Peripheral Reset */
#define CR_EXTRST        0x08 /* External Reset */

#define SR_URSTS         0x01 /* User Reset Status */
#define SR_BODSTS        0x02 /* Brownout Detection Status */
#define SR_NRSTL         0x10000 /* NRST Level */

#ifndef AT91_RSTC_ERR_DEBUG
#define AT91_RSTC_ERR_DEBUG 0
#endif

static uint64_t at91_rstc_mem_read(void *opaque, hwaddr addr, unsigned int size)
{
    AT91RSTCState *s = opaque;

    debug_log_mem_read(AT91_RSTC_ERR_DEBUG, __func__, addr, "", size);

    switch (addr) {
    case RSTC_SR:
        return s->sr;
    case RSTC_MR:
        return s->mr;
    default:
        return 0;
    }
}

static void at91_rstc_mem_write(void *opaque, hwaddr addr, uint64_t val64, unsigned int size)
{
    AT91RSTCState *s = opaque;
    uint32_t value = val64; 

    debug_log_mem_write(AT91_RSTC_ERR_DEBUG, __func__, addr, "", val64, size);

    if ((value >> 24) != WRITE_KEY)
    {
      debug_log(AT91_RSTC_ERR_DEBUG, __func__, "Incorrect key, aborting write");
      return;
    }

    switch (addr) {
      case RSTC_CR:
        debug_log_bool(AT91_RSTC_ERR_DEBUG, __func__, "Reset processor", value & CR_PROCRST);
        break;
      case RSTC_MR:
        debug_log_bool(AT91_RSTC_ERR_DEBUG, __func__, "User Reset enabled", value & SR_URSTS);    
        s->mr = value;
        break;
    }
}

static void at91_rstc_reset(DeviceState *dev)
{
    AT91RSTCState *s = AT91_RSTC(dev);

    debug_log_reset(AT91_RSTC_ERR_DEBUG, __func__);

    s->sr = 0x0;
    s->mr = 0x0;
}

static const MemoryRegionOps at91_rstc_ops = {
    .read = at91_rstc_mem_read,
    .write = at91_rstc_mem_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_at91_rstc = {
    .name = TYPE_AT91_RSTC,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32(cr, AT91RSTCState),
        VMSTATE_UINT32(sr, AT91RSTCState),
        VMSTATE_UINT32(mr, AT91RSTCState),
        VMSTATE_END_OF_LIST()
    }
};

static void at91_rstc_init(Object *obj)
{
    AT91RSTCState *s = AT91_RSTC(obj);

    memory_region_init_io(&s->mmio, obj, &at91_rstc_ops, s,
                          TYPE_AT91_RSTC, RSTC_SIZE);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);

}

static void at91_rstc_realize(DeviceState *dev, Error **errp)
{
    debug_log_realize(AT91_RSTC_ERR_DEBUG, __func__);

}

static void at91_rstc_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);


    dc->reset   = at91_rstc_reset;
    dc->vmsd    = &vmstate_at91_rstc;
    dc->desc    = "at91 reset controller";
    dc->realize = at91_rstc_realize;
}

static const TypeInfo at91_rstc_info = {
    .name          = TYPE_AT91_RSTC,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(AT91RSTCState),
    .instance_init = at91_rstc_init,
    .class_init    = at91_rstc_class_init,
};

static void at91_rstc_register_types(void)
{
    type_register_static(&at91_rstc_info);
}

type_init(at91_rstc_register_types)
