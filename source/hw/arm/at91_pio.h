/*
 * AT91 Parallel I/O Controller
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 */
 
 
// PIO definitions

#define PA0 				0			// pioa	
#define PIOA_PA0_PWM0		PA0+32  	// Peripheral A 
#define PIOA_PA0_TIOA0		PA0+64  	// Peripheral B

#define PA1 				1			// pioa	
#define PIOA_PA1_PWM1		PA1+32  	// Peripheral A 
#define PIOA_PA1_TIOB0		PA1+64  	// Peripheral B

#define PA2 				2			// pioa	
#define PIOA_PA2_PWM2		PA1+32  	// Peripheral A 
#define PIOA_PA2_SCK0		PA1+64  	// Peripheral B

#define PA3 				3			// pioa	
#define PIOA_PA3_TWD		PA3+32  	// Peripheral A 
#define PIOA_PA3_NPCS3		PA3+64  	// Peripheral B 

#define PA4 				4			// pioa	
#define PIOA_PA4_TWCK		PA4+32  	// Peripheral A 
#define PIOA_PA4_TCLK0		PA4+64  	// Peripheral B 

#define PA5 				5			// pioa	
#define PIOA_PA5_RXD0		PA5+32  	// Peripheral A 
#define PIOA_PA5_NPCS3		PA5+64  	// Peripheral B 

#define PA6 				6			// pioa	
#define PIOA_PA6_TXD0		PA6+32  	// Peripheral A 
#define PIOA_PA6_PCK0		PA6+64  	// Peripheral B 

#define PA7 				7			// pioa	
#define PIOA_PA7_RTS0		PA7+32  	// Peripheral A 
#define PIOA_PA7_PWM3		PA7+64  	// Peripheral B 

#define PA8 				8			// pioa	
#define PIOA_PA8_CTS0		PA8+32  	// Peripheral A 
#define PIOA_PA8_ADTRG		PA8+64  	// Peripheral B 

#define PA9 				9			// pioa	
#define PIOA_PA9_DRXD		PA9+32  	// Peripheral A 
#define PIOA_PA9_NPCS1		PA9+64  	// Peripheral B 
 
#define PA10 				10			// pioa	
#define PIOA_PA10_DTXD		PA10+32  	// Peripheral A
#define PIOA_PA10_NPCS2		PA10+64  	// Peripheral B

#define PA11 				11			// pioa	
#define PIOA_PA11_NPCS0		PA11+32  	// Peripheral A
#define PIOA_PA11_PWM0		PA11+64  	// Peripheral B
 
#define PA12 				12			// pioa	
#define PIOA_PA12_MISO		PA12+32  	// Peripheral A
#define PIOA_PA12_PWM1		PA12+64  	// Peripheral B
 
#define PA13	 			13			// pioa	
#define PIOA_PA13_MOSI		PA13+32  	// Peripheral A
#define PIOA_PA13_PWM2		PA13+64  	// Peripheral B
 
#define PA14	 			14			// pioa	
#define PIOA_PA14_SPCK		PA14+32  	// Peripheral A 
#define PIOA_PA14_PWM3		PA14+64  	// Peripheral B

#define PA30 				30			// pioa	
#define PIOA_PA30_IRQ1		PA30+32  	// Peripheral A
#define PIOA_PA30_NPCS2		PA30+64  	// Peripheral B

#define PIO_PINS        32  

typedef struct AT91PIOState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    MemoryRegion mmio;
    qemu_irq out[PIO_PINS * 3];
    qemu_irq parent_irq;
    uint32_t per;
    uint32_t pdr;    
    uint32_t psr;
    uint32_t oer;
    uint32_t odr;
    uint32_t osr;
    uint32_t ifer;
    uint32_t ifdr;
    uint32_t ifsr;
    uint32_t sodr;
    uint32_t codr;    
    uint32_t odsr;    
    uint32_t pdsr;
    uint32_t ier;
    uint32_t idr;
    uint32_t imr;
    uint32_t isr;
    uint32_t mder;
    uint32_t mddr; 
    uint32_t mdsr;
    uint32_t ppudr;
    uint32_t ppuer;
    uint32_t ppusr;
    uint32_t asr;
    uint32_t bsr;
    uint32_t absr;
    uint32_t ower; 
    uint32_t owdr;    
    uint32_t owsr;
    /* Mask of unknown state of PIO pins, needed for pull-up resistor
       implementation */
    uint32_t unknown_state;
} AT91PIOState;


#define TYPE_AT91_PIO "at91-pio"
#define AT91_PIO(obj) \
    OBJECT_CHECK(AT91PIOState, (obj), TYPE_AT91_PIO)
       