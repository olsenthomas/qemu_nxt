/*
 * AT91 Periodic Interval Timer Controller
 *
 * Copyright (c) 2009 Filip Navara, 2016 Thomas Olsen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/

typedef struct AT91PITState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    MemoryRegion mmio;
    qemu_irq irq;
    ptimer_state *timer;
    uint32_t mr;
    uint32_t sr;
    uint32_t pvir;
    uint32_t piir;
    uint32_t picnt;
} AT91PITState;

#define TYPE_AT91_PIT "at91-pit"
#define AT91_PIT(obj) \
    OBJECT_CHECK(AT91PITState, (obj), TYPE_AT91_PIT)

