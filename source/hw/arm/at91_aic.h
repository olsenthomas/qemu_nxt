/*
 * AT91 Advanced Interrupt Controller.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/


typedef struct AT91AICState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/    
    MemoryRegion mmio;
    qemu_irq parent_irq;
    qemu_irq parent_fiq;
    uint32_t smr[32];
    uint32_t svr[32];
    uint32_t ipr;
    uint32_t imr;
    uint32_t cisr;
    uint32_t spu;
    uint32_t dcr;
    uint32_t ffsr;
    uint8_t stack_irq[8];
    uint8_t stack_pri[8];
    int8_t stack_pos;
} AT91AICState;

#define TYPE_AT91_AIC "at91-aic"
#define AT91_AIC(obj) \
    OBJECT_CHECK(AT91AICState, (obj), TYPE_AT91_AIC)
