/*
 * AT91 Memory Controller
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/



typedef struct AT91MCState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    MemoryRegion mmio;
    MemoryRegion *sram;
    MemoryRegion *sram_alias;      
    MemoryRegion *flash;       
    MemoryRegion *flash_alias;
    uint32_t rcr;
    uint32_t asr;
    uint32_t aasr;
    uint32_t fmr0;
    uint32_t fcr0;
    uint32_t fsr0;
    uint32_t fmr1;
    uint32_t fcr1;
    uint32_t fsr1;               
} AT91MCState;

#define AT91_SRAM_ADDRESS               0x200000
#define AT91_SRAM_ADDRESS_AFTER_REMAP   0x0
#define AT91_SRAM_SIZE                  0x10000 // 65536 b = 64 Kb 
#define AT91_FLASH_ADDRESS              0x100000
#define AT91_FLASH_ADDRESS_AFTER_RESET  0x0
#define AT91_FLASH_SIZE                 0x40000  // 262144 = 256 Kb 

#define TYPE_AT91_MC "at91-mc"
#define AT91_MC(obj) \
    OBJECT_CHECK(AT91MCState, (obj), TYPE_AT91_MC)
