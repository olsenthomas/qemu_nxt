/*
 * AT91 SPI (and  PDC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "qemu/log.h"
#include "at91_spi.h"
#include "debug.h"

#ifndef AT91_SPI_ERR_DEBUG
#define AT91_SPI_ERR_DEBUG 0
#endif

const char* offsetToRegister(uint64_t offset)
{
	switch (offset & (SPI_SIZE - 1) ) 
   	{    
    	case SPI_CR:
    		return "SPI_CR";
    	case SPI_MR:
    		return "SPI_MR";
    	case SPI_RDR:
    		return "SPI_RDR";
    	case SPI_TDR:
    		return "SPI_TDR";   	
    	case SPI_SR:
    		return "SPI_SR";
    	case SPI_IER:
    		return "SPI_IER";
    	case SPI_IDR:
    		return "SPI_IDR";		
    	case SPI_IMR:
    		return "SPI_IMR";
    	case SPI_CSR0:
    		return "SPI_CSR0";
   	case SPI_CSR1:
    		return "SPI_CSR1";
    	case SPI_CSR2:
    		return "SPI_CSR2";								
    	case SPI_CSR3:
    		return "SPI_CSR3";		
    	case SPI_TPR:
    		return "SPI_TPR";		
    	case SPI_TCR:
    		return "SPI_TCR";
    	case SPI_TNPR:
    		return "SPI_TNPR";		
    	case SPI_TNCR:
    		return "SPI_TNCR";
    	case SPI_PTCR:
    		return "SPI_PTCR";		    			   	    	
    	default:
    		return "SPI_??";
   	}
   	
	return "SPI_??";
}

static void at91_spi_reset(DeviceState *dev)
{
	AT91SPIState *s = AT91_SPI(dev);
	
	s->MR = 0x0;
	s->TDR = 0x0;
	s->RDR = 0x0;
	s->IMR = 0x0;	
	s->CSR0 = 0x0;
	s->CSR1 = 0x0;
	s->CSR2 = 0x0;
	s->CSR3 = 0x0;		
	s->isTDREmpty = false;
	s->shiftRegister = 0x0;
	s->isShiftRegisterEmpty = false;
	s->isSPIEnabled = false;
		
	if (!s->isSoftwareReset)
	{		
		s->TPR = 0x0;
		s->TCR = 0x0;
		s->TNPR = 0x0;
		s->TNCR = 0x0;
		s->isReceiverTransferEnabled = false;
		s->isTransmitterTransferEnabled = false;			
	}
	
	s->isSoftwareReset = false;	
	
	//fprintf(stderr, "%s: SPI_SR value='0x%x' \n", __FUNCTION__, s->SR);	
}


static uint64_t at91_spi_mem_read(void *opaque, hwaddr addr, unsigned int size)
{
   AT91SPIState *s = opaque;
   uint32_t value = 0x0;
   //fprintf(stderr, "%s: offset='0x%x' \n", __FUNCTION__, offset);

   debug_log_mem_read(AT91_SPI_ERR_DEBUG, __func__, addr, offsetToRegister(addr), size);

   
    switch (addr) 
    {    
    	case SPI_CR:    	
    		hw_error("%s: SPI_CR is write only", __FUNCTION__);
     		break; 
    	case SPI_SR:    	    	
    		value = ((s->isShiftRegisterEmpty && s->isTDREmpty)? SPI_TXEMPTY : 0x0 ) |
    				( s->isTDREmpty? SPI_TDRE : 0x0 ) |
    				( s->isSPIEnabled? SPI_SPIENS : 0x0)
    				; 
    		//fprintf(stderr, "%s:SPI_SR value='0x%x' \n", __FUNCTION__, value);		
    		break;		
     	case SPI_TDR:
     		hw_error("%s: SPI_TDR is write only", __FUNCTION__);
     		break;
     	case SPI_CSR0:
    		value = s->CSR0;
    		break;
    	case SPI_CSR1:
    		value = s->CSR1;
    		break;
    	case SPI_CSR2:
    		value = s->CSR2;
    		break;		
    	case SPI_CSR3:
    		value = s->CSR3;
    		break;
     	case SPI_TPR:
    		value = s->TPR;
    		break;
    	case SPI_TCR:
    		value = s->TCR;
    		break;
    	case SPI_PTSR:
    		hw_error("%s: SPI_PTSR is not implemented!!!!!", __FUNCTION__);
    		break;	
    	case SPI_PTCR:
    		hw_error("%s: SPI_PTCR is read only", __FUNCTION__);
    		break; 	 	
   	default:
   		hw_error("%s: offset ='0x%lu' is not implemented", __FUNCTION__, addr);
    	        break;
    }
    
    return value;
}

static void at91_spi_mem_write(void *opaque, hwaddr addr,
                                uint64_t val64, unsigned int size)
{
    AT91SPIState *s = opaque;
    uint32_t value = val64;

    debug_log_mem_write(AT91_SPI_ERR_DEBUG, __func__, addr, offsetToRegister(addr), val64, size);
    
    switch (addr) 
    {   
    	case SPI_CR:
			if (value & SPI_SPIEN)
			{
				s->isSPIEnabled = true;
				s->isTDREmpty = true;
				s->isShiftRegisterEmpty = true;					
			}		
			if (value & SPI_SPIDIS) // SPIDIS takes precedence to SPIEN if both bits are 1.	
			{
				s->isSPIEnabled = false;
				s->isTDREmpty = false;
				s->isShiftRegisterEmpty = false;		
			}
			if (value & SPI_LASTXFER)
				hw_error("%s: SPI_LASTXFER is not implemented!!!", __FUNCTION__);	 					    		
    		// SPI software reset
    		if (value & SPI_SWRST)
    		{
    			s->isSoftwareReset = true;
    			at91_spi_reset(opaque);    
    		}
    		break;
    	case SPI_MR:
    		if (value & SPI_PS)
    			hw_error("%s: SPI_PS (Variable Peripheral Select) -  is not implemented!!!", __FUNCTION__);
    		if (value & SPI_PCSDEC)
    			hw_error("%s: SPI_PCSDEC (Chip Select Decoder) -  is not implemented!!!", __FUNCTION__);    		    		
    		s->MR = value;     		
    		break;
    	case SPI_TDR:
    		if (~(s->MR & SPI_PS) && (value & SPI_TPCS))
				hw_error("%s: Setting Peripheral Chip Select(PCS) in SPI_TDR has no effect when running Fixed Peripheral Select (PS=0)", __FUNCTION__);       	
     		s->TDR = value;
     		if (s->isShiftRegisterEmpty)
     		{
     			s->shiftRegister = value;
     			s->isShiftRegisterEmpty = false;
     			s->isTDREmpty = true;	
     		}
     		else
     			s->isTDREmpty = false;	
     			
     		break;
     	case SPI_SR:
     		hw_error("%s: SPI_SR is read only", __FUNCTION__);
     		break;	
    	case SPI_IMR:
     		hw_error("%s: SPI_IMR is read only", __FUNCTION__);
     		break;
	case SPI_CSR0 ... SPI_CSR3 :
	
		if (value & SPI_CSAAT)
			hw_error("%s: Chip Select Active After Transfer (CSAAT) is not implemented", __FUNCTION__);
		
		if ((value & SPI_SCBR) == 0x0 )
			hw_error("%s: Serial Clock Baud Rate (SCBR). Triggering a transfer while SCBR is at 0 can lead to unpredictable results", __FUNCTION__);
					
			
		switch (addr)
		{
			case SPI_CSR0:
			s->CSR0 = value;
			break;
		case SPI_CSR1:
			s->CSR1 = value;
			break;
		case SPI_CSR2:
			s->CSR2 = value;
			break;		
		case SPI_CSR3:
			s->CSR3 = value;
			break;			
		}
		break;
			
    	case SPI_TPR:  	
    		s->TPR = value;
    		break;
    	case SPI_TCR:
    		s->TCR = value;
    		break;
    	case SPI_PTSR:
    		hw_error("%s: SPI_PTSR is read only", __FUNCTION__);
    		break; 	
    	case SPI_PTCR:
    		if (value & PDC_RXTEN)
    			s->isReceiverTransferEnabled = true;
    		if (value & PDC_RXTDIS)
    			s->isReceiverTransferEnabled = false;
    		if (value & PDC_TXTEN)
    			s->isTransmitterTransferEnabled = true;
    		if (value & PDC_TXTDIS)
    			s->isTransmitterTransferEnabled = false;  
    		  		
    		if (s->isTransmitterTransferEnabled && s->isSPIEnabled)
    		{
    			// !!! spi tdr transfer is implemented here
    			// should be moved later!!!!    			    			
    			// chip 2 is hardcoded here!!!									
				
			// Because of hardware error CSR0 register 
			// specifies the number of bits to transmit
			// no matter which chip is selected!!! 
			if ((s->CSR0 & SPI_BITS))
				hw_error("%s: only 8 bit transfers are implemented", __FUNCTION__);
			
			s->isShiftRegisterEmpty = false;
			s->isTDREmpty = false;
			
			while (s->TCR != 0x0)
			{				
				uint8_t v = 0;
				cpu_physical_memory_read(s->TPR, &v, 1);	
				//if (v)
					//printf("%s: v ='0x%x' tpr ='0x%x' tcr = '%d' \n", __FUNCTION__, v, s->TPR, s->TCR );						
				qemu_set_irq(s->out[PIN_SPI_NPCS2], 0);			   				
				for (int i = 7; i > -1; i--)
 					qemu_set_irq(s->out[PIN_SPI_MOSI], (v & (1 << i))? 1 : 0);					
				qemu_set_irq(s->out[PIN_SPI_NPCS2], 1);
				s->TCR -= 1;
				s->TPR += 1; 
			}
			
			s->isShiftRegisterEmpty = true;
			s->isTDREmpty = true;
    		}	
				  		    			    			
    		break; 	   		    		
    	default:
    		hw_error("%s: offset ='0x%lu' is not implemented", __FUNCTION__, addr);
        	return;
    }
    
}


static const MemoryRegionOps at91_spi_ops = {
    .read = at91_spi_mem_read,
    .write = at91_spi_mem_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_at91_spi = {
    .name = TYPE_AT91_SPI,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32(CR, AT91SPIState),
        VMSTATE_UINT32(MR, AT91SPIState),
        VMSTATE_UINT32(RDR, AT91SPIState),
        VMSTATE_UINT32(TDR, AT91SPIState),
        VMSTATE_UINT32(SR, AT91SPIState),
        VMSTATE_UINT32(IER, AT91SPIState),
        VMSTATE_UINT32(IDR, AT91SPIState),
        VMSTATE_UINT32(IMR, AT91SPIState),
        VMSTATE_UINT32(CSR0, AT91SPIState),
        VMSTATE_UINT32(CSR1, AT91SPIState),
        VMSTATE_UINT32(CSR2, AT91SPIState),
        VMSTATE_UINT32(CSR3, AT91SPIState),
        VMSTATE_UINT32(TPR, AT91SPIState),
        VMSTATE_UINT32(TCR, AT91SPIState),
        VMSTATE_UINT32(TNPR, AT91SPIState),
        VMSTATE_UINT32(TNCR, AT91SPIState),
        VMSTATE_UINT32(PTCR, AT91SPIState),
        VMSTATE_UINT32(PTSR, AT91SPIState),
        VMSTATE_END_OF_LIST()
    }
};

static void at91_spi_init(Object *obj)
{
    AT91SPIState *s = AT91_SPI(obj);
    DeviceState *dev = DEVICE(obj);

    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->parent_irq);
    qdev_init_gpio_out(dev, s->out, 7);
	
    s->isSoftwareReset = false;

    memory_region_init_io(&s->mmio, obj, &at91_spi_ops, s,
                          TYPE_AT91_SPI, SPI_SIZE);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);

}

static void at91_spi_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset = at91_spi_reset;
    dc->vmsd = &vmstate_at91_spi;
}


static const TypeInfo at91_spi_info = {
    .name          = TYPE_AT91_SPI,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(AT91SPIState),
    .instance_init = at91_spi_init,
    .class_init    = at91_spi_class_init,
};

static void at91_spi_register_types(void)
{
    type_register_static(&at91_spi_info);
}

type_init(at91_spi_register_types)
