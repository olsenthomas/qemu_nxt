/*
 * AT91 Power Management Controller
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#define PIN_AT91_PMC_PCK0 0
#define PIN_AT91_PMC_PCK1 1
#define PIN_AT91_PMC_PCK2 2

#include "hw/sysbus.h"

typedef struct AT91PMCState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    MemoryRegion mmio;
    qemu_irq parent_irq;
    qemu_irq out[3];
    uint32_t mck_freq;
    uint32_t scer;
    uint32_t scdr;
    uint32_t scsr;
    uint32_t pcer;
    uint32_t pcdr;
    uint32_t pcsr;
    uint32_t mor;
    uint32_t mcfr;
    uint32_t plla;
    uint32_t pllb;
    uint32_t mckr;
    uint32_t pckr[3];
    uint32_t ier;
    uint32_t idr; 
    uint32_t imr;
    uint32_t sr;
} AT91PMCState;

#define TYPE_AT91_PMC "at91-pmc"
#define AT91_PMC(obj) \
    OBJECT_CHECK(AT91PMCState, (obj), TYPE_AT91_PMC)
