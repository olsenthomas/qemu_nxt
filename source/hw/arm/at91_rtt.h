/*
 * AT91 Real-time Timer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "hw/ptimer.h"

typedef struct AT91RTTState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    MemoryRegion mmio;
    qemu_irq irq;
    ptimer_state *timer;
    uint32_t mr;
    uint32_t ar;
    uint32_t vr;
    uint32_t sr;
} AT91RTTState;

#define TYPE_AT91_RTT "at91-rtt"
#define AT91_RTT(obj) \
    OBJECT_CHECK(AT91RTTState, (obj), TYPE_AT91_RTT)

