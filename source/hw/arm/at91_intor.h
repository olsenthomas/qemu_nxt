/*
 * AT91 Interrupt Logic OR
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/

typedef struct AT91IntOrState {
    /*< private >*/
    SysBusDevice parent_obj;
    /*< public >*/
    MemoryRegion mmio;
    qemu_irq parent_irq;
    uint32_t sources;
} AT91IntOrState;

#define TYPE_AT91_INTOR "at91-intor"
#define AT91_INTOR(obj) \
    OBJECT_CHECK(AT91IntOrState, (obj), TYPE_AT91_INTOR)
