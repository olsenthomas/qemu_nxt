/*
 * AT91 Parallel I/O Controller
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 */

/* TODO: Glitch-filter, multi-driver (ie. open drain) */

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "at91_pio.h"
#include "debug.h"

/*
 * Input/Output GPIO pins:
 * 32x PIO device
 * 32x peripheral A
 * 32x peripheral B
 */

#define PIO_SIZE        0x200

#define PIO_PER         0x00 /* PIO Enable Register */
#define PIO_PDR         0x04 /* PIO Disable Register */
#define PIO_PSR         0x08 /* PIO Status Register */
#define PIO_OER         0x10 /* Output Enable Register */
#define PIO_ODR         0x14 /* Output Disable Register */
#define PIO_OSR         0x18 /* Output Status Register */
#define PIO_IFER        0x20 /* Input Filter Enable Register */
#define PIO_IFDR        0x24 /* Input Filter Disable Register */
#define PIO_IFSR        0x28 /* Input Filter Status Register */
#define PIO_SODR        0x30 /* Set Output Data Register */
#define PIO_CODR        0x34 /* Clear Output Data Register */
#define PIO_ODSR        0x38 /* Output Data Status Register */
#define PIO_PDSR        0x3c /* Pin Data Status Register */
#define PIO_IER         0x40 /* Interrupt Enable Register */
#define PIO_IDR         0x44 /* Interrupt Disable Register */
#define PIO_IMR         0x48 /* Interrupt Mask Register */
#define PIO_ISR         0x4c /* Interrupt Status Register */
#define PIO_MDER        0x50 /* Multi-driver Enable Register */
#define PIO_MDDR        0x54 /* Multi-driver Disable Register */
#define PIO_MDSR        0x58 /* Multi-driver Status Register */
#define PIO_PPUDR       0x60 /* Pull-up Disable Register */
#define PIO_PPUER       0x64 /* Pull-up Enable Register */
#define PIO_PPUSR       0x68 /* Pull-up Status Register */
#define PIO_ASR         0x70 /* Select A Register */
#define PIO_BSR         0x74 /* Select B Register */
#define PIO_ABSR        0x78 /* AB Select Status Register */
#define PIO_OWER        0xa0 /* Output Write Enable Register */
#define PIO_OWDR        0xa4 /* Output Write Disable Register */
#define PIO_OWSR        0xa8 /* Output Write Status Register */

#ifndef AT91_PIO_ERR_DEBUG
#define AT91_PIO_ERR_DEBUG 0
#endif

static void at91_pio_set_pin(void *opaque, int pin, int level)
{
    AT91PIOState *s = opaque;
    int mask = 1 << (pin % PIO_PINS);
    int input_set = pin / PIO_PINS;
    int output_set = (s->absr & mask)?2:1;
    uint32_t saved_pdsr;

	//fprintf(stderr, "%s: pin= '%d' level='%d' input_set='%d' output_set='%d' mask='0x%x' absr='0x%x' \n", __FUNCTION__, pin, level, input_set, output_set, mask, s->absr);

    if (input_set == 0) {
        /* PIO pin -> Peripheral / IO */

        /* Skip input if output mode is enabled for the pin */
        if (s->osr & mask)
            return;

        if (s->psr & mask) {
            saved_pdsr = s->pdsr;
            s->pdsr &= ~mask;
            if (level == -1) {
                s->unknown_state |= mask;
            } else if (level) {
                s->unknown_state &= ~mask;
                s->pdsr |= mask;
            }
            if (saved_pdsr != s->pdsr) {
                s->isr |= mask;
                qemu_set_irq(s->parent_irq, !!(s->isr & s->imr));
            }
        } else {
            qemu_set_irq(s->out[PIO_PINS + (output_set * PIO_PINS)], level);
        }
    } else {
        /* Peripheral -> PIO pin */                
        if ((~s->psr & mask) && input_set == output_set) 
        {   
            //fprintf(stderr, "%s: pin= '%d' level='%d' \n", __FUNCTION__, pin % PIO_PINS, level);      	
            qemu_set_irq(s->out[pin % PIO_PINS], level);
        }
    }

}

static uint64_t at91_pio_mem_read(void *opaque, hwaddr addr,
                                     unsigned int size)
{
    AT91PIOState *s = opaque;
    int isr;

    debug_log_mem_read(AT91_PIO_ERR_DEBUG, __func__, addr, "", size);

    switch (addr) {
    case PIO_PSR:
        return s->psr;
    case PIO_OSR:
        return s->osr;
    case PIO_IFSR:
        return s->ifsr;
    case PIO_ODSR:
        return s->odsr;
    case PIO_PDSR:
      // fprintf(stderr, "%s: PIO_PDSR value='0x%x' \n", __FUNCTION__, ((s->pdsr & ~s->unknown_state) | (s->ppusr & s->unknown_state)));
      // fprintf(stderr, "%s: PIO_PPUSR value='0x%x' \n", __FUNCTION__, s->ppusr);
      // fprintf(stderr, "%s: unknown state value='0x%x' \n", __FUNCTION__, s->unknown_state);
        return
            (s->pdsr & ~s->unknown_state) |
            (s->ppusr & s->unknown_state);
    case PIO_IMR:
        return s->imr;
    case PIO_ISR:
        isr = s->isr;
        s->isr = 0;
        qemu_set_irq(s->parent_irq, 0);
        return isr;
    case PIO_MDSR:
        return s->mdsr;
    case PIO_PPUSR:
        return s->ppusr;
    case PIO_ABSR:
        return s->absr;
    case PIO_OWSR:
        return s->owsr;
    default:
        return 0;
    }
}




static void at91_pio_mem_write(void *opaque, hwaddr addr,
                                uint64_t val64, unsigned int size)
{
    AT91PIOState *s = opaque;
    uint32_t value = val64;
    int i;

    debug_log_mem_write(AT91_PIO_ERR_DEBUG, __func__, addr, "", val64, size);

    switch (addr) {
    case PIO_PER:
        s->psr |= value;
        break;
    case PIO_PDR:
        s->psr &= ~value;
        break;
    case PIO_OER:
        s->osr |= value;
        break;
    case PIO_ODR:
        s->osr &= ~value;
        break;
    case PIO_IFER:
        s->ifsr |= value;
        break;
    case PIO_IFDR:
        s->ifsr &= ~value;
        break;
    case PIO_SODR:
        s->odsr |= value;
        for (i = 0; i < PIO_PINS; i++)
            if (value & (1 << i) & s->osr)
            {  
            	 qemu_set_irq(s->out[i], 1);
            	 //fprintf(stderr, "%s:PIO_SODR i='%d' value='1' \n", __FUNCTION__, i);
            }
        // Set the level on the pin if output pin
        // TODO -- Need to test for pio clock tooo
        s->pdsr |= s->osr & value;
        s->unknown_state &= ~(s->osr & value);
        break;
    case PIO_CODR:
        s->odsr &= ~value;
        for (i = 0; i < PIO_PINS; i++)
            if (value & (1 << i) & s->osr)
            {
                qemu_set_irq(s->out[i], 0);
               // fprintf(stderr, "%s:PIO_CODR i='%d' value='0' \n", __FUNCTION__, i);
            }
        // Set the level on the pin if output pin
        // TODO -- Need to test for pio clock tooo
        s->pdsr &= ~(s->osr & value);
        s->unknown_state &= ~(s->osr & value);
        break;
    case PIO_ODSR:
        s->odsr = (s->odsr & ~s->owsr) | (value & s->owsr);
        for (i = 0; i < PIO_PINS; i++)
            if (s->owsr & (1 << i))
                qemu_set_irq(s->out[i], !!(value & (1 << i)));
        break;
    case PIO_IER:
        s->imr |= value;
        break;
    case PIO_IDR:
        s->imr &= ~value;
        break;
    case PIO_MDER:
        s->mdsr |= value;
        break;
    case PIO_MDDR:
        s->mdsr &= ~value;
        break;
    case PIO_PPUER:
        s->ppusr |= value;
        break;
    case PIO_PPUDR:
        s->ppusr &= ~value;
        break;
    case PIO_ASR:        
        s->absr &= ~value;
        break;
    case PIO_BSR:
        s->absr |= value;
        break;
    case PIO_OWER:
        s->owsr |= value;
        break;
    case PIO_OWDR:
        s->owsr &= ~value;
        break;
    default:
        return;
    }
}

static void at91_pio_reset(DeviceState *dev)
{    
    AT91PIOState *s = AT91_PIO(dev);

    s->psr              = 0xffffffff;
    s->osr              = 0x0;
    s->ifsr             = 0x0;
    s->odsr             = 0x0;
    s->pdsr             = 0x0;
    s->imr              = 0x0;
    s->isr              = 0x0;
    s->mdsr             = 0x0;
    s->ppusr            = 0x0;
    s->absr             = 0x0;
    s->owsr             = 0x0;
    s->unknown_state    = 0xffffffff;
}

static const MemoryRegionOps at91_pio_ops = {
    .read = at91_pio_mem_read,
    .write = at91_pio_mem_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_at91_pio = {
    .name = TYPE_AT91_PIO,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
    VMSTATE_UINT32(per, AT91PIOState),
    VMSTATE_UINT32(pdr, AT91PIOState),    
    VMSTATE_UINT32(psr, AT91PIOState),
    VMSTATE_UINT32(oer, AT91PIOState),
    VMSTATE_UINT32(odr, AT91PIOState),
    VMSTATE_UINT32(osr, AT91PIOState),
    VMSTATE_UINT32(ifer, AT91PIOState),
    VMSTATE_UINT32(ifdr, AT91PIOState),
    VMSTATE_UINT32(ifsr, AT91PIOState),
    VMSTATE_UINT32(sodr, AT91PIOState),
    VMSTATE_UINT32(codr, AT91PIOState),    
    VMSTATE_UINT32(odsr, AT91PIOState),    
    VMSTATE_UINT32(pdsr, AT91PIOState),
    VMSTATE_UINT32(ier, AT91PIOState),
    VMSTATE_UINT32(idr, AT91PIOState),
    VMSTATE_UINT32(imr, AT91PIOState),
    VMSTATE_UINT32(isr, AT91PIOState),
    VMSTATE_UINT32(mder, AT91PIOState),
    VMSTATE_UINT32(mddr, AT91PIOState), 
    VMSTATE_UINT32(mdsr, AT91PIOState),
    VMSTATE_UINT32(ppudr, AT91PIOState),
    VMSTATE_UINT32(ppuer, AT91PIOState),
    VMSTATE_UINT32(ppusr, AT91PIOState),
    VMSTATE_UINT32(asr, AT91PIOState),
    VMSTATE_UINT32(bsr, AT91PIOState),
    VMSTATE_UINT32(absr, AT91PIOState),
    VMSTATE_UINT32(ower, AT91PIOState), 
    VMSTATE_UINT32(owdr, AT91PIOState),    
    VMSTATE_UINT32(owsr, AT91PIOState),        
    VMSTATE_END_OF_LIST()
    }
};

static void at91_pio_init(Object *obj)
{
    AT91PIOState *s = AT91_PIO(obj);
    DeviceState *dev = DEVICE(obj);

    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->parent_irq);

    qdev_init_gpio_in(dev, at91_pio_set_pin, PIO_PINS * 3);
    qdev_init_gpio_out(dev, s->out, PIO_PINS * 3);

    memory_region_init_io(&s->mmio, obj, &at91_pio_ops, s, TYPE_AT91_PIO, PIO_SIZE);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);

}

static void at91_pio_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset = at91_pio_reset;
    dc->vmsd = &vmstate_at91_pio;
}

static const TypeInfo at91_pio_info = {
    .name          = TYPE_AT91_PIO,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(AT91PIOState),
    .instance_init = at91_pio_init,
    .class_init    = at91_pio_class_init,
};

static void at91_pio_register_types(void)
{
    type_register_static(&at91_pio_info);
}

type_init(at91_pio_register_types)
