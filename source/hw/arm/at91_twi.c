/*
 * AT91 two-wire interface (I2C)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "at91_twi.h"
#include "debug.h"

#ifndef AT91_TWI_ERR_DEBUG
#define AT91_TWI_ERR_DEBUG 1
#endif

#define AT91_TWI_CR				0x00
#define AT91_TWI_MMR			0x04
#define AT91_TWI_IADR			0x0C
#define AT91_TWI_CWGR			0x10
#define AT91_TWI_SR				0x20	
#define AT91_TWI_IER			0x24
#define AT91_TWI_IDR			0x28
#define AT91_TWI_IMR			0x2C
#define AT91_TWI_RHR			0x30
#define AT91_TWI_THR			0x34

#define AT91_TWI_CR_SWRST		0x80 // Software Reset


#define AT91_TWI_SIZE	0x7C

const char* at91_twi_AddrToRegister(hwaddr addr)
{	   	
    switch (addr) 
    { 
      case AT91_TWI_CR:
        return "Control Register";
      case AT91_TWI_MMR:
        return "Master Mode Register";
      case AT91_TWI_IADR:
        return "Internal Address Register";
      case AT91_TWI_CWGR:
        return "Clock Waveform Generator Register";
      case AT91_TWI_SR:
        return "Status Register";
      case AT91_TWI_IER:
        return "Interrupt Enable Register";
      case AT91_TWI_IDR:
        return "Interrupt Disable Register";
      case AT91_TWI_IMR:
        return "Interrupt Mask Register";
      case AT91_TWI_RHR:
        return "Receive Holding Register";
      case AT91_TWI_THR:
        return "Transmit Holding Register";                     
    }

	return "Reserved";
}

static void at91_twi_reset(DeviceState *dev)
{
    AT91TWIState *s = AT91_TWI(dev);

    s->mmr  = 0x0;
    s->iadr = 0x0;
    s->cwgr = 0x0;
    s->sr   = 0x8;
    s->imr  = 0x0;
    s->idr  = 0x0;
    s->rhr  = 0x0;
    s->thr  = 0x0;

	debug_log_reset(AT91_TWI_ERR_DEBUG, __func__);		
}

static void at91_twi_set_pin(void *opaque, int pin, int level)
{
    
    //fprintf(stderr, "%s: pin= '%d' level='%d' \n", __FUNCTION__, pin, level);
   
}

static uint64_t at91_twi_mem_read(void *opaque, hwaddr addr, unsigned int size)
{ 
    AT91TWIState *s = opaque;

    debug_log_mem_read(AT91_TWI_ERR_DEBUG, __func__, addr, at91_twi_AddrToRegister(addr), size);

	switch (addr) 
    {  
        case AT91_TWI_CR:
          hw_error("%s: AT91_TWI_CR is read only", __FUNCTION__);
        case AT91_TWI_SR:
          return s->sr;
		default:			
			break;    		 	
    }
	
    return 0;
}

static void at91_twi_mem_write(void *opaque, hwaddr addr, uint64_t val64, unsigned int size)
{
   AT91TWIState *s = opaque;
    
   debug_log_mem_write(AT91_TWI_ERR_DEBUG, __func__, addr, at91_twi_AddrToRegister(addr), val64, size);       

   switch (addr) 
    {  

        case AT91_TWI_CR:
          s->cr = val64;
          if (s->cr & AT91_TWI_CR_SWRST)
          {
            at91_twi_reset(DEVICE(s));  
          }
        case AT91_TWI_IER:
          s->ier = val64;
        case AT91_TWI_IDR:
          s->idr = val64;  
		default:			
			break;    		 	
    }

}

static const MemoryRegionOps at91_twi_ops = {
    .read = at91_twi_mem_read,
    .write = at91_twi_mem_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_at91_twi = {
    .name = TYPE_AT91_TWI,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32(cr, AT91TWIState),
        VMSTATE_UINT32(mmr, AT91TWIState),
        VMSTATE_UINT32(iadr, AT91TWIState),
        VMSTATE_UINT32(cwgr, AT91TWIState),
        VMSTATE_UINT32(sr, AT91TWIState),
        VMSTATE_UINT32(ier, AT91TWIState),
        VMSTATE_UINT32(idr, AT91TWIState),
        VMSTATE_UINT32(imr, AT91TWIState),
        VMSTATE_UINT32(rhr, AT91TWIState),
        VMSTATE_UINT32(thr, AT91TWIState),
        VMSTATE_END_OF_LIST()
    }
};

static void at91_twi_init(Object *obj)
{

    AT91TWIState *s = AT91_TWI(obj);
    DeviceState *dev = DEVICE(obj);

	sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->parent_irq);
	qdev_init_gpio_out(dev, s->out, 2);
	qdev_init_gpio_in(dev, at91_twi_set_pin, 2);  

    memory_region_init_io(&s->mmio, obj, &at91_twi_ops, s, TYPE_AT91_TWI, AT91_TWI_SIZE);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);  

}

static void at91_twi_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset = at91_twi_reset;
    dc->vmsd = &vmstate_at91_twi;
}

static const TypeInfo at91_twi_info = {
    .name          = TYPE_AT91_TWI,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(AT91TWIState),
    .instance_init = at91_twi_init,
    .class_init    = at91_twi_class_init,
};

static void at91_twi_register_types(void)
{
    type_register_static(&at91_twi_info);
}

type_init(at91_twi_register_types)