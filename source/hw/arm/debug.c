/*
 * Debugging functions
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

 #include "debug.h"
 #include "hw/sysbus.h"

void debug_log_mem_read(int enabled, const char* function_name, hwaddr addr, const char* regiser_name, unsigned int size)
{
  if (enabled > 0)
  {
    qemu_log("%s: address=0x%" HWADDR_PRIx "(%s) size=0x%x\n", function_name, addr, regiser_name, size);
  }
}

void debug_log_mem_write(int enabled, const char* function_name, hwaddr addr, const char* regiser_name, uint64_t value, unsigned int size)
{
  if (enabled > 0)
  {
    qemu_log("%s: address=0x%" HWADDR_PRIx "(%s) value=0x%lx size=0x%x\n", function_name, addr, regiser_name, value, size);
  }
}

void debug_log_reset(int enabled, const char* function_name)
{
  debug_log(enabled, function_name, "resetting controller");
}

void debug_log_realize(int enabled, const char* function_name)
{  
  debug_log(enabled, function_name, "realize controller");
}

void debug_log(int enabled, const char* function_name, const char* text)
{
  if (enabled > 0)
  {
    qemu_log("%s: %s\n", function_name, text);
  }
}

void debug_log_bool(int enabled, const char* function_name, const char* text, int value)
{
  if (enabled > 0)
  {
    qemu_log("%s: %s = %s\n", function_name, text, value ? "true" : "false" );
  }
}

void debug_log_command_not_implemented(int enabled, const char* function_name, uint32_t command)
{
  if (enabled > 0)
  {
    qemu_log("%s: command ='0x%x' not implemented!!!\n", function_name, command);
  }
}
