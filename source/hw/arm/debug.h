/*
 * Debugging functions
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

 #include "qemu/osdep.h"
 #include "hw/sysbus.h"
 #include "qemu/log.h"

 // debug_log_mem_read(AT91_AIC_ERR_DEBUG, __func__, addr, "", size);
 void debug_log_mem_read(int enabled, const char* function_name, hwaddr addr, const char* regiser_name, unsigned int size);

 // debug_log_mem_write(AT91_AIC_ERR_DEBUG, __func__, addr, "", val64, size);
 void debug_log_mem_write(int enabled, const char* function_name, hwaddr addr, const char* regiser_name, uint64_t value, unsigned int size);

 //  debug_log_reset(AT91_AIC_ERR_DEBUG, __func__);
 void debug_log_reset(int enabled, const char* function_name);

 //  debug_log_realize(AT91_AIC_ERR_DEBUG, __func__);
 void debug_log_realize(int enabled, const char* function_name);

 //  debug_log(AT91_AIC_ERR_DEBUG, __func__, "bla bla bla");
 void debug_log(int enabled, const char* function_name, const char* text);

 void debug_log_bool(int enabled, const char* function_name, const char* text, int value);

 void debug_log_command_not_implemented(int enabled, const char* function_name, uint32_t command);