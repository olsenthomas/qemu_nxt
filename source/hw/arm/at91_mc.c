/*
 * AT91 Memory Controller
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "hw/hw.h"
#include "at91_mc.h"
#include "qemu/log.h"
#include "qapi/error.h"
#include "exec/address-spaces.h"
#include "debug.h"

#include "cpu.h"
#include "exec/exec-all.h"

#ifndef AT91_MC_ERR_DEBUG
#define AT91_MC_ERR_DEBUG 0
#endif

#define MC_RCR	0x00
#define MC_ASR	0x04
#define MC_AASR	0x08
#define MC_FMR0	0x60
#define MC_FCR0	0x64
#define MC_FSR0	0x68
#define MC_FMR1	0x70
#define MC_FCR1	0x74
#define MC_FSR1	0x78

#define MC_SIZE	0x100 // 256 Bytes/64 registers



static void at91_mc_reset(DeviceState *dev)
{	
	debug_log_reset(AT91_MC_ERR_DEBUG, __func__);

	AT91MCState *s = AT91_MC(dev);
	s->rcr  = 0x0;
	s->asr  = 0x0;
	s->aasr = 0x0;
	s->fmr0 = 0x0;
    s->fmr1 = 0x0;

    if (memory_region_is_mapped(s->sram_alias) == true)
    {
      memory_region_del_subregion(get_system_memory(), s->sram_alias);  
    }

    if (memory_region_is_mapped(s->flash_alias) == false)
    {
      memory_region_add_subregion(get_system_memory(), AT91_FLASH_ADDRESS_AFTER_RESET, s->flash_alias);
    }
}

static void at91_mc_realize(DeviceState *dev, Error **errp)
{
    debug_log_realize(AT91_MC_ERR_DEBUG, __func__);
}

static uint64_t at91_mc_mem_read(void *opaque, hwaddr addr, unsigned int size)
{ 
    debug_log_mem_read(AT91_MC_ERR_DEBUG, __func__, addr, "", size);

	switch (addr) 
    {          
    	case MC_FSR0:
    		fprintf(stdout, "AT91 flash is ready");
    		return 0x1; // flash ready!!!
		default:
			hw_error("%s: offset='0x%lx' \n", __FUNCTION__, addr);
			break;    		 	
    }
	
    return 0;
}

static void at91_mc_mem_write(void *opaque, hwaddr addr, uint64_t val64, unsigned int size)
{
  debug_log_mem_write(AT91_MC_ERR_DEBUG, __func__, addr, "", val64, size); 
  AT91MCState *s = opaque;
  uint32_t value = val64; 

  switch (addr) 
  { 
    case MC_FMR0:
        s->fmr0 = value;
        break; 
	case MC_RCR:
	    if (singlestep)
	    {
	      hw_error("%s: Remapping of flash or ram address does not work in single step mode because it needs the pipeline to jump to the right location\n", __FUNCTION__);
	    }
	     
		s->rcr = value ^ s->rcr;
		if (s->rcr & 0x1) 
		{
		  debug_log(AT91_MC_ERR_DEBUG, __func__, "Ram is mapped to address 0x0");
		  if (memory_region_is_mapped(s->flash_alias) == true)
		  {
		    memory_region_del_subregion(get_system_memory(), s->flash_alias);
		  }
		  if (memory_region_is_mapped(s->sram_alias) == false) 
		  {
		    memory_region_add_subregion(get_system_memory(), AT91_SRAM_ADDRESS_AFTER_REMAP, s->sram_alias);
		  }         
		}
		else 
		{
		  debug_log(AT91_MC_ERR_DEBUG, __func__, "Flash is mapped to address 0x0");
		}
		break;
	default:
		hw_error("%s: offset='0x%lx' value='0x%lx' \n", __FUNCTION__, addr, val64);
		break;    		 	
  }  
}

static const MemoryRegionOps at91_mc_ops = {
    .read = at91_mc_mem_read,
    .write = at91_mc_mem_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static const VMStateDescription vmstate_at91_mc = {
    .name = TYPE_AT91_MC,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
    VMSTATE_UINT32(rcr, AT91MCState),
    VMSTATE_UINT32(asr, AT91MCState),
    VMSTATE_UINT32(aasr, AT91MCState),
    VMSTATE_UINT32(fmr0, AT91MCState),
    VMSTATE_UINT32(fcr0, AT91MCState),
    VMSTATE_UINT32(fsr0, AT91MCState),
    VMSTATE_UINT32(fmr1, AT91MCState),
    VMSTATE_UINT32(fcr1, AT91MCState),
    VMSTATE_UINT32(fsr1, AT91MCState),
    VMSTATE_END_OF_LIST()
    }
};


static void at91_mc_init(Object *obj)
{

    AT91MCState *s = AT91_MC(obj);

    s->sram        = g_new0(MemoryRegion, 1);
    s->sram_alias  = g_new0(MemoryRegion, 1);
    s->flash       = g_new0(MemoryRegion, 1);
    s->flash_alias = g_new0(MemoryRegion, 1);

    memory_region_init_ram(s->sram, NULL, "at91.ram", AT91_SRAM_SIZE, &error_fatal);    
    memory_region_init_alias(s->sram_alias, NULL, "at91.ram.alias", s->sram, 0, AT91_SRAM_SIZE);    
    vmstate_register_ram_global(s->sram);
    memory_region_add_subregion(get_system_memory(), AT91_SRAM_ADDRESS, s->sram);    

    memory_region_init_ram(s->flash, NULL, "at91.flash", AT91_FLASH_SIZE, &error_fatal);
    memory_region_init_alias(s->flash_alias, NULL, "at91.flash.alias", s->flash, 0, AT91_FLASH_SIZE);
    memory_region_set_readonly(s->flash_alias, true);        
    vmstate_register_ram_global(s->flash);  
    memory_region_set_readonly(s->flash, true);  
    memory_region_add_subregion(get_system_memory(), AT91_FLASH_ADDRESS, s->flash);   

    memory_region_init_io(&s->mmio, obj, &at91_mc_ops, s,
                          TYPE_AT91_MC, MC_SIZE);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);
}

static void at91_mc_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset   = at91_mc_reset;
    dc->realize = at91_mc_realize;
    dc->desc    = "AT91 Memory Controller";
    dc->vmsd    = &vmstate_at91_mc;
}

static const TypeInfo at91_mc_info = {
    .name          = TYPE_AT91_MC,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(AT91MCState),
    .instance_init = at91_mc_init,
    .class_init    = at91_mc_class_init,
};

static void at91_mc_register_types(void)
{
    type_register_static(&at91_mc_info);
}

type_init(at91_mc_register_types)
