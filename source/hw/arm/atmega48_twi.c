/*
 * ATmega48 two-wire interface
 *
 * Copyright (c) 2016 Thomas Olsen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
*/


#include "qemu/osdep.h"
#include "hw/sysbus.h"
#include "atmega48_twi.h"
#include "debug.h"

#ifndef ATMEGA48_TWI_ERR_DEBUG
#define ATMEGA48_TWI_ERR_DEBUG 0
#endif

static void atmega48_twi_reset(DeviceState *dev)
{
	debug_log_reset(ATMEGA48_TWI_ERR_DEBUG, __func__);	
}

static void atmega48_twi_set_pin(void *opaque, int pin, int level)
{
	ATmega48TWIState *s = opaque;
	//fprintf(stderr, "%s: pin= '%d' level='%d' \n", __FUNCTION__, pin, level);
	// TWD is initially pulled to 1 because we let the line float
    qemu_set_irq(s->out[0], 1);
}


static void atmega48_twi_init(Object *obj)
{
	ATmega48TWIState *s = ATMEGA48_PIT(obj);
	DeviceState *dev = DEVICE(obj);
	qdev_init_gpio_out(dev, s->out, 2);
	qdev_init_gpio_in(dev, atmega48_twi_set_pin, 2);
}


static void atmega48_twi_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset = atmega48_twi_reset;
    //dc->vmsd = &vmstate_atmega48_twi;
}

static const TypeInfo atmega48_twi_info = {
    .name          = TYPE_ATMEGA48_TWI,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(ATmega48TWIState),
    .instance_init = atmega48_twi_init,
    .class_init    = atmega48_twi_class_init,
};

static void atmega48_twi_register_types(void)
{
    type_register_static(&atmega48_twi_info);
}

type_init(atmega48_twi_register_types)