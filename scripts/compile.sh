#!/bin/bash

source qemuPath.sh

echo "Copying files to the QEMU folder"

# Ultra Chip 1601
cp -v ./../source/hw/display/uc1601.h $QEMU_HOME/hw/display/
cp -v ./../source/hw/display/uc1601.c $QEMU_HOME/hw/display/
grep -q -F 'obj-$(CONFIG_NXT) += uc1601.o' $QEMU_HOME/hw/display/Makefile.objs || echo 'obj-$(CONFIG_NXT) += uc1601.o' >> $QEMU_HOME/hw/display/Makefile.objs

# AT91 SPI
cp -v ./../source/hw/arm/at91_spi.h $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/at91_spi.c $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += at91_spi.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += at91_spi.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# AT91 PMC
cp -v ./../source/hw/arm/at91_pmc.h $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/at91_pmc.c $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += at91_pmc.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += at91_pmc.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# AT91 PIO
cp -v ./../source/hw/arm/at91_pio.h $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/at91_pio.c $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += at91_pio.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += at91_pio.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# AT91 TWI
cp -v ./../source/hw/arm/at91_twi.h $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/at91_twi.c $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += at91_twi.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += at91_twi.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# AT91 AIC
cp -v ./../source/hw/arm/at91_aic.h $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/at91_aic.c $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += at91_aic.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += at91_aic.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# AT91 Real time timer
cp -v ./../source/hw/arm/at91_rtt.c $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/at91_rtt.h $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += at91_rtt.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += at91_rtt.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# AT91 MC
cp -v ./../source/hw/arm/at91_mc.c $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/at91_mc.h $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += at91_mc.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += at91_mc.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# AT91 Interrupt Logic OR
cp -v ./../source/hw/arm/at91_intor.h $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/at91_intor.c $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += at91_intor.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += at91_intor.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# AT91 reset controller
cp -v ./../source/hw/arm/at91_rstc.c $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/at91_rstc.h $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += at91_rstc.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += at91_rstc.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# AT91 Periodic Interval Timer
cp -v ./../source/hw/arm/at91_pit.c $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/at91_pit.h $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += at91_pit.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += at91_pit.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# atmega48 TWI
cp -v ./../source/hw/arm/atmega48_twi.c $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/atmega48_twi.h $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += atmega48_twi.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += atmega48_twi.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# debug code
cp -v ./../source/hw/arm/debug.c $QEMU_HOME/hw/arm/
cp -v ./../source/hw/arm/debug.h $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += debug.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += debug.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# Lego NXT mindstorms board
cp -v ./../source/hw/arm/nxt.c $QEMU_HOME/hw/arm/
grep -q -F 'obj-$(CONFIG_NXT) += nxt.o' $QEMU_HOME/hw/arm/Makefile.objs || echo 'obj-$(CONFIG_NXT) += nxt.o' >> $QEMU_HOME/hw/arm/Makefile.objs

# Enable the NXT
grep -q -F 'CONFIG_NXT=y' $QEMU_HOME/default-configs/arm-softmmu.mak || echo 'CONFIG_NXT=y' >> $QEMU_HOME/default-configs/arm-softmmu.mak

make --directory=$QEMU_HOME defconfig
make --directory=$QEMU_HOME

# update default-configs/arm-softmmu.mak 
# https://fulcronz27.wordpress.com/2014/06/06/qemu-enabling-a-new-build-targetoption/
