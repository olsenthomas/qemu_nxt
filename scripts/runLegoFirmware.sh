#!/bin/bash

source "./qemuPath.sh"

# debug:  -d in_asm,cpu

$QEMU_HOME/arm-softmmu/qemu-system-arm -kernel ./../firmware/firmware1.05.rfw -M nxt -d cpu,in_asm -D ./nxt.log -monitor stdio -singlestep


# /home/thomaso/StuffToSave/thomas/navara/arm-softmmu/qemu-system-arm -nographic -kernel nxt.bin -M nxt -m 1 -d in_asm,cpu

